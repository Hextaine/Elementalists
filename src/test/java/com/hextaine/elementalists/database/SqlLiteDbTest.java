package com.hextaine.elementalists.database;

import static org.junit.Assert.fail;

import org.junit.Test;

public class SqlLiteDbTest {
    @Test
    public void testSetup() {
        SqlLiteDb db = new SqlLiteDb("jdbc:sqlite:"
        + SqlLiteDbTest.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(1)
        + "../../src/test/resources/testSetupDb.db");
        try {
            db.connectToDB();
        } catch (Exception e) {
            fail(e.toString());
        }
    }
    
    @Test
    public void testGetPlayerStats() {
        System.out.println(SqlLiteDbTest.class.getProtectionDomain().getCodeSource().getLocation().getPath()
                + "../../src/test/resources/testSetupDb.db");
    }
    
    @Test
    public void testSyncPlayerDataToDB() {

    }
}
