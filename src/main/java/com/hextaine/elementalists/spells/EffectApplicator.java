package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/21/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;

/**
 * Interface to govern how spells that apply effects, affect players with their effects.
 * Take note people - This is how you use 'Affect' vs 'Effect'!
 */
public interface EffectApplicator {
    /**
     * Apply the effect to the given entity with a set power and duration (modified in the
     * cast method) with the applicator of the provided caster.
     *
     * @param caster    the player who cast the spell that is applying the effect
     * @param target    the entity recieving the effect
     * @param power     the strength of the effect
     * @param duration  the duration the effect will span
     */
    void applyEffect(PlayerStats caster, EntityStats target, double power, int duration);
    
    /**
     * Cancel the effects of the current spell from the entity.
     * <p>
     * This is supposed to be more useful for effects that create relationships between the caster
     * and affected entity such as a damage redirection spell that should cancel when either entity
     * dies.
     *
     * @param affectedEntity    the entity that is currently afflicted by a spell-based effect
     */
    void cancelEffect(EntityStats affectedEntity);
    
    /**
     * Cancel the effect affecting multiple entities all at once.
     * <p>
     * The use of this would be, again, for when a spell creates a relationship between the caster
     * and the affected entities such if a caster absorbs part of the damage from multiple entities.
     *
     * @param afflictedEntities
     */
    void cancelEffects(EntityStats[] afflictedEntities);
}
