package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/21/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;

public interface TargetedSpell {
    /**
     * Given the caster of the spell, get the target the spell would hit.
     *
     * @param caster    caster of the spell
     * @return  the target the spell should hit when cast by the caster
     */
    EntityStats getTarget(PlayerStats caster);
}
