package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/22/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;

/**
 * Interface to govern how Area Of Effect (AoE) spells should function
 */
public interface AreaSpell {
    /**
     * Resolve the targets of the spell given whoever might cast the spell.
     *
     * @param caster    the caster of the spell
     * @return  all entities ({@link EntityStats}) who are in the spell's AoE
     */
    EntityStats[] getAffectedTargets(PlayerStats caster);
}
