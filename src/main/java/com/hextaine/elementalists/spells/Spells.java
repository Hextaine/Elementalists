package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 1/3/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.air.AetherGrasp;
import com.hextaine.elementalists.spells.air.AirClap;
import com.hextaine.elementalists.spells.air.AirPellet;
import com.hextaine.elementalists.spells.earth.EarthenCoating;
import com.hextaine.elementalists.spells.earth.InfinitePatience;
import com.hextaine.elementalists.spells.earth.TechtonicSlam;
import com.hextaine.elementalists.spells.fire.Burnout;
import com.hextaine.elementalists.spells.fire.Combust;
import com.hextaine.elementalists.spells.fire.Nova;

public enum Spells {

    AETHERGRASP(null), AIRCLAP(null), AIRPELLET(null),

    EARTHENCOATING(null), INFINITEPATIENCE(null), TECHTONICSLAM(null),

    COMBUST(null), NOVA(null), BURNOUT(null);

    private Spell spell;

    Spells(Spell spell) {
        this.spell = spell;
    }

    public static void initialize() {
        AETHERGRASP.spell = new AetherGrasp();
        AIRCLAP.spell = new AirClap();
        AIRPELLET.spell = new AirPellet();
        EARTHENCOATING.spell = new EarthenCoating();
        INFINITEPATIENCE.spell = new InfinitePatience();
        TECHTONICSLAM.spell = new TechtonicSlam();
        COMBUST.spell = new Combust();
        NOVA.spell = new Nova();
        BURNOUT.spell = new Burnout();
    }

    public static Spells getSpellByName(String name) {
        if (name == null)
            return null;

        name = name.replace(" ", "");

        for (Spells s : Spells.values()) {
            if (s.getSpellHandler().getName().replace(" ", "").equalsIgnoreCase(name)) {
                return s;
            }
        }

        return null;
    }

    public Spell getSpellHandler() {
        return spell;
    }

    public void cast(PlayerStats caster, SpellModifier modifier, boolean setOnCooldown) {
        if (spell.cast(caster, modifier))
            if (setOnCooldown)
                spell.setOnCooldown(caster);
    }

    public static void flavorItem(ItemStack itemStack, Spells spell) {
        if (itemStack == null)
            return;

        ItemMeta spellItemMeta = itemStack.getItemMeta();
        spellItemMeta.setDisplayName(spell.getSpellHandler().getName());

        ArrayList<String> spellDesc = new ArrayList<String>();
        spellDesc.add(spell.getSpellHandler().getDescription());
        while (spellDesc.get(spellDesc.size() - 1).length() > 40) {
            int endIdx = spellDesc.get(spellDesc.size() - 1).indexOf(" ", 40);
            if (endIdx < 1) {
                break;
            }

            String newLine = spellDesc.get(spellDesc.size() - 1).substring(endIdx + 1);
            spellDesc.set(spellDesc.size() - 1, spellDesc.get(spellDesc.size() - 1).substring(0, endIdx));
            spellDesc.add(newLine);
        }
        spellItemMeta.setLore(spellDesc);

        itemStack.setItemMeta(spellItemMeta);
    }
}
