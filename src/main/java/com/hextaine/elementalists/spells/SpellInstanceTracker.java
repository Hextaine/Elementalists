package com.hextaine.elementalists.spells;

import org.bukkit.Bukkit;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.util.ConditionalCheck;

public class SpellInstanceTracker {
    private boolean isEnded;
    private boolean isScheduledToCancel;
    private int schedulerID;
    private ConditionalCheck selfCancelCheck;
    private int ticker;

    public SpellInstanceTracker() {
        this.schedulerID = -1;
    }

    public SpellInstanceTracker(ConditionalCheck selfCancelCheck) {
        this();
        this.selfCancelCheck = selfCancelCheck;
    }

    public boolean getIsScheduledToCancel() {
        return isScheduledToCancel;
    }

    public void setScheduledToCancel(boolean willCancel) {
        isScheduledToCancel = willCancel;
    }

    public boolean isEnded() {
        return isEnded;
    }

    public boolean canTriggerSelfCancel() {
        return !isEnded && !isScheduledToCancel && this.selfCancelCheck.meetsSelfCancelConditions(this);
    }

    public void setDelayedEnd(long delayTicks) {
        if (isScheduledToCancel) {
            return;
        }
        this.isScheduledToCancel = true;

        Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
            public void run() {
                if (schedulerID > 0) {
                    Bukkit.getScheduler().cancelTask(schedulerID);
                }
                isEnded = true;
            }
        }, delayTicks);
    }

    public void endSpell() {
        if (this.schedulerID > 0) {
            Bukkit.getScheduler().cancelTask(this.schedulerID);
        }

        this.isEnded = true;
    }

    public void setSchedulerID(int id) {
        this.schedulerID = id;
    }

    public int getSchedulerID() {
        return this.schedulerID;
    }

    public void incrementTicker() {
        this.ticker++;
    }

    public void setTicker(int tick) {
        this.ticker = tick;
    }

    public int getTicker() {
        return this.ticker;
    }
}
