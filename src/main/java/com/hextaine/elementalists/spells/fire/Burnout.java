package com.hextaine.elementalists.spells.fire;
/*
 * Created by Hextaine on 1/6/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.AreaSpell;
import com.hextaine.elementalists.spells.Spell;
import com.hextaine.elementalists.spells.SpellModifier;
import com.hextaine.elementalists.spells.SpellType;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;

public class Burnout extends Spell implements AreaSpell {

    public Burnout() {
        super("Burnout",
                "Extinguish any nearby enemies, dealing a moderate amount of damage to anyone extinguished",
                SpellType.FIRE,
                "elementalists.spell.burnout");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier modifier) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "The Elemental Academy hasn't approved you for this spell yet!");
            return false;
        }

        boolean didAnything = false;

        for (EntityStats target : getAffectedTargets(caster)) {
            if (target.isOnFire()) {
                target.ignite(0);
                target.damage(7, caster, this.getName());

                didAnything = true;
            }
        }

        return didAnything;
    }

    // TODO Doesn't register armor stands as valid targets
    @Override
    public EntityStats[] getAffectedTargets(PlayerStats caster) {
        return TargetCalculation.getNearbyTargets(caster, 8, false);
    }
}
