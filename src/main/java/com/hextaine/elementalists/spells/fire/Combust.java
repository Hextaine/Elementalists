package com.hextaine.elementalists.spells.fire;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.*;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

public class Combust extends Spell implements TargetedSpell, EffectApplicator {
    public Combust() {
        super("Combust",
                "Ignite an enemy or the block you look at instantaneously. If the enemy or block is " +
                        "already alight, cause the target to explode dealing moderate damage and send sparks flying " +
                        "outwards that will ignite any player who touches them.",
                SpellType.FIRE,
                "elementalists.spell.combust");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier modifier) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "The Elemental Academy has not approved you to use this spell yet!");
            return false;
        }

        EntityStats target = getTarget(caster);
        if (target == null) {
            Block b = TargetCalculation.getTargetBlock(caster.getEntity(), 50);
            final Block aboveTarget = b.getRelative(0, 1, 0);
            if (!aboveTarget.getType().isSolid()) {
                final Material oldMat = aboveTarget.getType();
                final BlockState state = aboveTarget.getState();
                aboveTarget.setType(Material.FIRE);
                Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
                    public void run() {
                        // TODO globally stored list of blocks changed, when to revert them and to what
                        // Make sure nothing else has changed the block
                        if (aboveTarget.getType().equals(oldMat) || aboveTarget.getType().equals(Material.AIR))
                            // force the block to return to it's previous state
                            state.update(true);
                    }
                }, 100L);
            }
        } else {
            applyEffect(caster, target, 1, (int) (4 * 20 * modifier.durationModifier));
        }

        return true;
    }

    @Override
    public void applyEffect(PlayerStats caster, EntityStats target, double power, int duration) {
        if (!target.isOnFire()) {
            target.ignite(duration);
        } else {
            target.damage(4, caster, this.getName());
            target.getEntity().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, target.getEntity().getLocation(), 4);

            for (EntityStats nearbyTarget : TargetCalculation.getNearbyTargets(caster, 4, false)) {
                nearbyTarget.ignite(duration);
            }
        }
    }

    @Override
    public EntityStats getTarget(PlayerStats caster) {
        return TargetCalculation.rayCastSingleTarget(caster, 40);
    }

    @Override
    public void cancelEffect(EntityStats affectedEntity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void cancelEffects(EntityStats[] afflictedEntities) {
        // TODO Auto-generated method stub

    }
}
