package com.hextaine.elementalists.spells.fire;
/*
 * Created by Hextaine on 1/5/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.*;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class Nova extends Spell implements AreaSpell, EffectApplicator {
    private static final int RANGE = 8;

    public Nova() {
        super("Nova",
                "Explode outwards brightly, blinding all nearby opponants looking at you and igniting any " +
                        "opponents who are in close proximity.",
                SpellType.FIRE,
                "elementalists.spell.nova");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier modifier) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "The Elemental Academy hasn't approved you to use this spell yet!");
            return false;
        }

        for (EntityStats target : getAffectedTargets(caster)) {
            applyEffect(caster, target, 1, (int) (2 * 20 * modifier.powerModifier));
        }

        return true;
    }

    @Override
    public void applyEffect(PlayerStats caster, EntityStats target, double power, int duration) {
        if (target.getEntity().getLocation().distanceSquared(caster.getEntity().getLocation()) < RANGE * RANGE / 2)
            target.ignite(duration);
        target.addEffect(new PotionEffect(PotionEffectType.BLINDNESS, duration, 1));
    }

    @Override
    public EntityStats[] getAffectedTargets(PlayerStats caster) {
        ArrayList<EntityStats> nearbyTargets = new ArrayList<EntityStats>();

        for (EntityStats entity : TargetCalculation.getNearbyTargets(caster, RANGE, false)) {
            if (entity.getEntity().getLocation().distanceSquared(caster.getEntity().getLocation()) >= RANGE * RANGE / 2) {
                for (EntityStats potentialTarget : TargetCalculation.rayCastAllTarget(caster.getEntity(), RANGE)) {
                    if (potentialTarget.getEntity().equals(caster.getEntity())) {
                        nearbyTargets.add(entity);
                    }
                }
            } else {
                nearbyTargets.add(entity);
            }
        }

        EntityStats[] targets = new EntityStats[nearbyTargets.size()];
        return nearbyTargets.toArray(targets);
    }

    @Override
    public void cancelEffect(EntityStats affectedEntity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void cancelEffects(EntityStats[] afflictedEntities) {
        // TODO Auto-generated method stub

    }
}
