package com.hextaine.elementalists.spells.air;

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.AreaSpell;
import com.hextaine.elementalists.spells.Spell;
import com.hextaine.elementalists.spells.SpellModifier;
import com.hextaine.elementalists.spells.SpellType;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

/**
 * Created by Hextaine on 12/27/2017.
 * Code is not for redistribution or any other use other than personal.
 * All questions can be directed to hextaine@zoho.com
 */
public class AirClap extends Spell implements AreaSpell {
    private double strength = 1;
    private int distance = 6;
    private double damage = 8;
    private double width = 3;
    
    private double velocityBalance = 0.8;
    
    public AirClap() {
        super("Air Clap",
                "Creates a line in extending from you where air ceases to exist" +
                        " forcing everything after a split second to crash back together" +
                        " and fill that gap.",
                SpellType.AIR,
                "elementalists.spell.airclap");
    }
    
    // TODO WAAAAAAAYYYY Overtuned
    @Override
    public boolean cast(PlayerStats caster, SpellModifier modifier) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "Insufficient Permissions!");
            return false;
        }
        
        int entitiesAffected = 0;
        double damage = 0;
        
        // Add in try catch for disaster case?
        Location line = null;
        Vector add = null;
        
        // TODO move into getAffectedTargets()
        // TODO Remove magic numbers
        for (Entity e : caster.getEntity().getNearbyEntities(distance - 0.2, distance - 0.2, distance - 0.2)) {
            double leg = e.getLocation().distance(caster.getEntity().getLocation());
            line = caster.getEntity().getLocation().add(caster.getEntity().getLocation().getDirection().
                    multiply(leg));
            
            if (e.getLocation().distanceSquared(line) > width * width * modifier.powerModifier)
                continue;
            
            entitiesAffected++;
            
            Vector delta = line.subtract(e.getLocation()).toVector();
            delta.multiply(velocityBalance);
            delta.multiply(modifier.powerModifier);
            
            add = caster.getEntity().getLocation().getDirection().multiply(leg / distance).multiply(1.2);
            
            delta.add(add);
            
            if (e instanceof LivingEntity) {
                line = caster.getEntity().getLocation().add(caster.getEntity().getLocation().getDirection().
                        multiply(leg));
                
                damage = ((width * width - e.getLocation().
                        distanceSquared(line)) / width * width) * this.damage * (0.8 * modifier.powerModifier + 0.2);
                ((LivingEntity)e).damage(damage);
            }
            
            e.setVelocity(delta);
        }
        
        return true;
    }
    
    // TODO
    @Override
    public EntityStats[] getAffectedTargets(PlayerStats caster) {
        return new EntityStats[0];
    }
}
