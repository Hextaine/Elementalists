package com.hextaine.elementalists.spells.air;

import com.hextaine.elementalists.afflictions.Affliction;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.ProjectileSpell;
import com.hextaine.elementalists.spells.Spell;
import com.hextaine.elementalists.spells.SpellModifier;
import com.hextaine.elementalists.spells.SpellType;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.Projectile;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.Particle.DustTransition;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Hextaine on 12/27/2017.
 * Code is not for redistribution or any other use other than personal.
 * All questions can be directed to hextaine@zoho.com
 */

public class AirPellet extends Spell implements ProjectileSpell {
    private double damage = 0.5;
    private double power = 5;

    public AirPellet() {
        super("Air Pellet",
                "Throw a pellet of solid air that deals a small bit of damage, but also slows the enemy for a bit.",
                SpellType.AIR,
                "elementalists.spell.airpellet");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier mod) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "Insufficient Permissions!");
            return false;
        }

        Projectile proj = getProjectileItem(caster);

        DustTransition dustData = new DustTransition(Color.fromRGB(255, 255, 255), Color.fromRGB(0, 0, 0), 1.0F);
        proj.spawnParticleProjectile(caster.getEntity().getEyeLocation(), 4, Particle.DUST_COLOR_TRANSITION, dustData);

        return true;
    }

    @Override
    public Projectile getProjectileItem(PlayerStats caster) {
        return new Projectile("Air Pellet", 40, 0.5, caster.getEntity(), SpellType.AIR, Color.GRAY,
                new Affliction(PotionEffectType.SLOW, (int) (0.5 * 20), 1), false);
    }
}
