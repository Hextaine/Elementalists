package com.hextaine.elementalists.spells.air;

import org.bukkit.Bukkit;

import com.hextaine.elementalists.afflictions.Affliction;
import com.hextaine.elementalists.afflictions.afflictiontypes.Frozen;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.*;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;

/**
 * Created by Hextaine on 12/26/2017.
 * Code is not for redistribution or any other use other than personal.
 * All questions can be directed to hextaine@zoho.com
 */
public class AetherGrasp extends Spell implements TargetedSpell, EffectApplicator {
    private int range = 30;
    private double damage = 2;
    private int duration = (int) (1.5 * 20);

    public AetherGrasp() {
        super("Grasp of the Aether",
                "Use the power of the Aether to hold a player still for a short time, even at large distances!",
                SpellType.AIR,
                "elementalists.spell.aethergrasp");
    }

    @Override
    public String getDescription() {
        return "Use the power of the Aether to hold a player still for a short time, even at large distances!";
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier mod) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "Insufficient Permissions!");
            return false;
        }

        EntityStats target = getTarget(caster);
        if (target == null) {
            // No target, but we still successfully cast the spell
            return true;
        }

        if (target.getEntity().getLocation().subtract(0, 0.1, 0).getBlock().getType().isSolid()) {
            target.getEntity().teleport(target.getEntity().getLocation().add(0, 0.5, 0));
        }

        applyEffect(caster, target, (int) Math.ceil(mod.powerModifier), duration);
        target.damage(damage * mod.powerModifier + 0.2, caster, this.getName());

        return true;
    }

    @Override
    public EntityStats getTarget(PlayerStats caster) {
        return TargetCalculation.rayCastSingleTarget(caster, range);
    }

    @Override
    public void applyEffect(PlayerStats caster, EntityStats targetStats, double power, int duration) {
        targetStats.addEffect(new Affliction(new Frozen(targetStats), duration, (int) power));
    }

    @Override
    public void cancelEffect(EntityStats affectedEntity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void cancelEffects(EntityStats[] afflictedEntities) {
        // TODO Auto-generated method stub

    }
}
