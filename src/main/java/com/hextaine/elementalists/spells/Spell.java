package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;

public abstract class Spell {
    private String name;
    private String description;
    private String permission;
    private SpellType type;
    private int energyCost;
    private int cooldown;

    public static final double PROJECTILETOLERANCE = 0.5;
    
    public Spell(String name, String description, SpellType type, String permission) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.permission = permission;
        
        cooldown = 3;
    }
    
    
    /**
     * Casts the spell from the perspective of the caster with the given modification to the cast.
     *
     * @param caster    the caster of the spell
     * @param modifier       how the spell should be modified when cast
     * @return  <code>true</code> if the spell was cast / handled or <code>false</code> if something
     *          went wrong
     */
    public abstract boolean cast(PlayerStats caster, SpellModifier modifier);
    
    public void setOnCooldown(PlayerStats caster) {
        // TODO
    }
    
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public SpellType getType() {
        return type;
    }

    public String getPermissionString() {
        return permission;
    }
}
