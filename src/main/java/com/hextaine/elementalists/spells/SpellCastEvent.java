package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/24/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SpellCastEvent extends Event implements Cancellable {
    public static final HandlerList handlers = new HandlerList();
    private Spell spell;
    private PlayerStats caster;
    private boolean cancelled;
    private SpellModifier modifier;
    
    public SpellCastEvent(Spell spell, PlayerStats caster) {
        this.spell = spell;
        this.caster = caster;
        cancelled = false;
        modifier = SpellModifier.REGULAR;
    }
    
    public Spell getSpell() {
        return spell;
    }
    
    public PlayerStats getCaster() {
        return caster;
    }

    public SpellModifier getModifier() {
        return modifier;
    }

    public void setModifier(SpellModifier modifier) {
        this.modifier = modifier;
    }

    public void setPowerModifier(double spellPower) {
        if (modifier == SpellModifier.REGULAR)
            if (spellPower < 1)
                this.modifier = SpellModifier.WEAKENED;
            else if (spellPower > 1)
                this.modifier = SpellModifier.EMPOWERED;

        this.modifier.powerModifier = spellPower;
    }

    public void setDurationModifier(double durationModifier) {
        if (modifier == SpellModifier.REGULAR)
            if (durationModifier < 1)
                this.modifier = SpellModifier.WEAKENED;
            else if (durationModifier > 1)
                this.modifier = SpellModifier.EMPOWERED;

        this.modifier.durationModifier = durationModifier;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHanderList() {
        return handlers;
    }
    
    @Override
    public boolean isCancelled() {
        return cancelled;
    }
    
    @Override
    public void setCancelled(boolean b) {
        cancelled = b;
    }
}
