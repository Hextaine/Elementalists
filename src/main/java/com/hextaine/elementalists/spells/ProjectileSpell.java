package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/27/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.util.Projectile;

/**
 * Interface that governs how spells that involve sending out projectiles should function.
 */
public interface ProjectileSpell {
    /**
     * Gets the projectile the spell will create when cast. This only has to be one of the basic
     * projectiles the spell creates if the spell, like Fracture (Ice Spell), creates multiple
     * projectiles.
     *
     * @return  the projectile the spell will spawn when cast (can be only particles)
     */
    Projectile getProjectileItem(PlayerStats caster);
}
