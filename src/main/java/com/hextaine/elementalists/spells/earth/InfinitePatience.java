package com.hextaine.elementalists.spells.earth;
/*
 * Created by Hextaine on 12/28/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.afflictions.Affliction;
import com.hextaine.elementalists.afflictions.afflictiontypes.Untargetable;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.*;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.potion.PotionEffectType;

public class InfinitePatience extends Spell implements TargetedSpell, EffectApplicator {
    
    public InfinitePatience() {
        super("Infinite Patience",
                "You encase yourself in stone causing immobility but protect " +
                        "yourself from all forms of damage and effects for the duration.",
                SpellType.EARTH,
                "elementalists.spell.infinitepatience");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier mod) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "You do not have permission to cast this spell!");
            return false;
        }
        
        applyEffect(caster, getTarget(caster), 1, 4 * 20);
        
        return false;
    }
    
    @Override
    public EntityStats getTarget(PlayerStats caster) {
        return caster;
    }
    
    @Override
    public void applyEffect(PlayerStats caster, EntityStats target, double power, int duration) {
        target.removeAllEffects();
        
        target.addEffect(new Affliction(new Untargetable(), (int) duration, 1));
        target.addEffect(new Affliction(PotionEffectType.DAMAGE_RESISTANCE, (int) duration, 64));
        
        // TODO Particle Generator
    }

    @Override
    public void cancelEffect(EntityStats affectedEntity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void cancelEffects(EntityStats[] afflictedEntities) {
        // TODO Auto-generated method stub
        
    }
}
