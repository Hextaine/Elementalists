package com.hextaine.elementalists.spells.earth;
/*
 * Created by Hextaine on 12/31/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.Spell;
import com.hextaine.elementalists.spells.SpellInstanceTracker;
import com.hextaine.elementalists.spells.SpellModifier;
import com.hextaine.elementalists.spells.SpellType;
import com.hextaine.elementalists.util.ConditionalCheck;
import com.hextaine.elementalists.util.KnockbackInteractor;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;

public class TechtonicSlam extends Spell implements ConditionalCheck {
    WeakHashMap<PlayerStats, SpellInstanceTracker> activeSpells;
    int id;

    public TechtonicSlam() {
        super("Techtonic Slam",
                "Throw your body forwards with the power of a rock slide. If you collide with any enemies " +
                        "before touching the ground, the enemy takes a moderate amount of damage and is thrown away " +
                        "from you with considerable force.",
                SpellType.EARTH,
                "elementalists.spell.techtonicslam");

        this.activeSpells = new WeakHashMap<>();
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier modifier) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "The Elemental Academy has not approved you to use this spell yet!");
            return false;
        }

        // Intentionally get entity directly so redirections don't apply
        Vector casterDirection = caster.getEntity().getLocation().getDirection();
        KnockbackInteractor.knockBackEntity(caster.getEntity(), casterDirection.setY(casterDirection.getY() + 0.5), 1.5, caster);

        final PlayerStats scheduledCaster = caster;
        int schedulerID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Elementalists.pl, new Runnable() {
            public void run() {
                SpellInstanceTracker spellInstance = activeSpells.get(scheduledCaster);
                if (spellInstance == null || spellInstance.isEnded()) {
                    if (spellInstance != null) {
                        spellInstance.endSpell();
                        activeSpells.remove(scheduledCaster);
                    }

                    return;
                }
                spellInstance.incrementTicker();

                Block blockBelowEntity = scheduledCaster.getEntity().getLocation().add(0, -0.5, 0).getBlock();
                if (blockBelowEntity.getType().isSolid() || scheduledCaster.getEntity().getVelocity().isZero()) {
                    if (spellInstance.canTriggerSelfCancel()) {
                        spellInstance.setDelayedEnd(10);
                    }
                }

                EntityStats[] possibleTargets = TargetCalculation.getCollidingEntities(scheduledCaster);
                if (possibleTargets.length > 0) {
                    activateHit(scheduledCaster, possibleTargets[0]);
                    return;
                }

            }
        }, 0L, 1L);

        if (activeSpells.containsKey(scheduledCaster)) {
            activeSpells.remove(scheduledCaster).endSpell();
        }

        SpellInstanceTracker spellInstance = new SpellInstanceTracker(this);
        spellInstance.setSchedulerID(schedulerID);
        activeSpells.put(scheduledCaster, spellInstance);

        return true;
    }

    private void activateHit(PlayerStats caster, EntityStats target) {
        caster.getEntity().setVelocity(new Vector(0, 0, 0));
        target.damage(7, caster, this.getName());

        Vector direction = target.getEntity().getLocation().subtract(caster.getEntity().getLocation()).toVector();
        KnockbackInteractor.knockBackEntity(target.getEntity(), direction.setY(direction.getY() + 0.5), 2, caster);

        activeSpells.remove(caster).endSpell();
    }

    public boolean meetsSelfCancelConditions(Object o) {
        if (!(o instanceof SpellInstanceTracker))
            return false;
        SpellInstanceTracker spellInstance = (SpellInstanceTracker) o;

        if (spellInstance.getTicker() > 10) {
            return true;
        }
        return false;
    }
}
