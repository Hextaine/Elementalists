package com.hextaine.elementalists.spells.earth;
/*
 * Created by Hextaine on 1/1/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.*;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.TargetCalculation;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EarthenCoating extends Spell implements EffectApplicator, TargetedSpell {
    private int duration = 20 * 60 * 2;

    public EarthenCoating() {
        super("Earthen Coating",
                "Coat yourself or an ally in a layer of soft soils and earth, padding the target from " +
                        "a good bit of damage and increasing their power for 2 minutes or until the health is warn " +
                        "away by damage.",
                SpellType.EARTH,
                "elementalists.spell.earthencoating");
    }

    @Override
    public boolean cast(PlayerStats caster, SpellModifier mod) {
        if (!caster.hasPermission(getPermissionString())) {
            Messenger.sendErrorMessage(caster.getEntity(), "The elemental academy hasn't approved you to use this spell");
            return false;
        }

        EntityStats target = getTarget(caster);
        if (target == null) {
            Messenger.sendErrorMessage(caster.getEntity(), "No target selected!");
            return false;
        }

        applyEffect(caster, target, (int) Math.ceil(mod.powerModifier), (int) Math.ceil(mod.durationModifier * duration));

        return true;
    }

    @Override
    public void applyEffect(PlayerStats caster, EntityStats target, double power, int duration) {
        target.addEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, duration, (int) power));
        target.addEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, duration, 1));
    }

    @Override
    public EntityStats getTarget(PlayerStats caster) {
        if (caster.getEntity().isSneaking()) {
            return caster;
        } else {
            return TargetCalculation.rayCastSingleTarget(caster, TargetCalculation.MAXRANGE);
        }
    }

    @Override
    public void cancelEffect(EntityStats affectedEntity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void cancelEffects(EntityStats[] afflictedEntities) {
        // TODO Auto-generated method stub

    }
}
