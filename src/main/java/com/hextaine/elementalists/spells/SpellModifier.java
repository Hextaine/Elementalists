package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/21/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

public enum SpellModifier {
    REGULAR(1, 1), EMPOWERED(1, 2), WEAKENED(1, 0.5), LENGTHEN(2, 1), SHORTEN(0.5, 1);
    
    public double durationModifier;
    public double powerModifier;
    SpellModifier(double durationModifier, double powerModifier) {
        this.durationModifier = durationModifier;
        this.powerModifier = powerModifier;
    }
}
