package com.hextaine.elementalists.spells;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

public enum SpellType {
    NONE, EARTH, AIR, WATER, FIRE;
}
