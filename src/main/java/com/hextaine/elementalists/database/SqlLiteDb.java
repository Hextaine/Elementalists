package com.hextaine.elementalists.database;
/*
 * Created by Hextaine on 1/11/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.UUID;

import com.hextaine.elementalists.Elementalists;

// TODO Separate the specific model loading into abstract class / interface so it can be handled by the model itself
public class SqlLiteDb extends AbstractDbHandler {
    private Connection dbConn = null;
    private String connURL;

    public SqlLiteDb() {
        this.connURL = "jdbc:sqlite:"
                + Elementalists.pl.getDataFolder().getAbsolutePath().toLowerCase().replace('\\', '/')
                + "/playerstats.db";
    }

    public SqlLiteDb(String connURL) {
        this.connURL = connURL;
    }

    public void connectToDB() throws Exception {
        // Don't do anything if we're already good to go
        if (dbConn != null && !dbConn.isClosed()) {
            return;
        }

        Connection connection = DriverManager.getConnection(this.connURL);
        if (connection == null) {
            // Elementalists.log.logError("Failed to create database due to an unknown
            // reason!");
            return;
        }

        dbConn = connection;
        setup();
    }

    void setup() throws Exception {
        // If the connection isn't null, it means at some point the database was already
        // setup
        if (dbConn == null || dbConn.isClosed())
            throw new Exception("no db to connect to");

        // NOTE: Could be dynamic?
        // Create table if it doesn't exist just to be safe with values as seen in the
        // PlayerStats class
        String sql = String.format("""
                CREATE TABLE IF NOT EXISTS %s (
                    playerStatId INT PRIMARY KEY,
                    uuid VARCHAR(64) UNIQUE NOT NULL,
                    kills INT,
                    deaths INT,
                    assists INT
                );""", this.tablePlayerStats);

        Statement stmt = dbConn.createStatement();
        if (stmt != null) {
            stmt.execute(sql);
            return;
        }

        throw new Exception("failed to create statement");
    }

    @Override
    public DbPlayerStats getPlayerStats(UUID playerUUID) throws Exception {
        DbPlayerStats playerStats = new DbPlayerStats(this, playerUUID);

        String query = String.format("""
                SELECT kills,deaths,assists
                FROM %s
                WHERE uuid=?
                """, this.tablePlayerStats);

        PreparedStatement prepStmt = dbConn.prepareStatement(query);
        prepStmt.setString(1, playerUUID.toString());

        ResultSet rslts = prepStmt.executeQuery();
        if (!rslts.next()) {
            // No results returned
            return playerStats;
        }

        playerStats.setKills(rslts.getInt("kills"));
        playerStats.setDeaths(rslts.getInt("deaths"));
        playerStats.setAssists(rslts.getInt("assists"));
        playerStats.setLoadedFromDB();

        rslts.close();

        return playerStats;
    }

    @Override
    public void syncPlayerDataToDB(DbPlayerStats playerStats) throws Exception {
        String queryToExec = null;
        Object[] args = null;
        if (playerStats.wasLoadedFromDB()) {
            queryToExec = String.format("""
                    UPDATE %s SET kills=?, deaths=?, assists=? WHERE uuid=?;
                    """, this.tablePlayerStats);
            args = new Object[] { playerStats.getKills(), playerStats.getDeaths(), playerStats.getAssists(),
                    playerStats.getPlayerUUID() };
        } else {
            queryToExec = String.format("""
                    INSERT INTO %s (uuid,kills,deaths,assists) VALUES (?,?,?,?);
                    """, this.tablePlayerStats);
            args = new Object[] { playerStats.getPlayerUUID(), playerStats.getKills(), playerStats.getDeaths(),
                    playerStats.getAssists() };
        }

        PreparedStatement prepStmt = dbConn.prepareStatement(queryToExec);
        int idx = 0;
        for (Object param : args) {
            prepStmt.setObject(idx, param);
            idx++;
        }
        prepStmt.execute();

        playerStats.setLoadedFromDB();
    }
}
