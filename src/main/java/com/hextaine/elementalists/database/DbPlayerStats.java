package com.hextaine.elementalists.database;

import java.util.UUID;

// TODO Dissolve into the PlayerStats class
public class DbPlayerStats {
    private AbstractDbHandler dbHandler;
    private UUID playerUUID;
    private int kills;
    private int deaths;
    private int assists;
    protected boolean loadedFromDB;

    DbPlayerStats(AbstractDbHandler dbHandler, UUID playerUUID) {
        this.playerUUID = playerUUID;
        this.dbHandler = dbHandler;
    }

    public DbPlayerStats(AbstractDbHandler dbHandler, UUID playerUUID, int kills, int deaths, int assists) {
        this(dbHandler, playerUUID);
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
        this.loadedFromDB = false;
    }

    public UUID getPlayerUUID() {
        return this.playerUUID;
    }

    public int getKills() {
        return this.kills;
    }
    public int getDeaths() {
        return this.deaths;
    }
    public int getAssists() {
        return this.assists;
    }
    public boolean wasLoadedFromDB() {
        return this.loadedFromDB;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }
    
    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }
    
    public void setAssists(int assists) {
        this.assists = assists;
    }
    // This is a 1-way set because there isn't a way for something to be unloaded from the DB without being deleted,
    // which shouldn't happen and leave this.
    protected void setLoadedFromDB() {
        this.loadedFromDB = true;
    }
}
