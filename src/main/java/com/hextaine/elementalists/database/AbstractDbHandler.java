package com.hextaine.elementalists.database;

import java.util.UUID;

public abstract class AbstractDbHandler {
    public String[] tablePlayerStatsColumns = new String[] { "kills", "deaths", "assists" };
    public String databaseNameGame = "game";
    public String tablePlayerStats = "player_stats";
    public String tablePlayerProgression = "player_progression";

    abstract void setup() throws Exception;

    public abstract void syncPlayerDataToDB(DbPlayerStats playerStats) throws Exception;

    public abstract DbPlayerStats getPlayerStats(UUID playerUUID) throws Exception;

    public abstract void connectToDB() throws Exception;
}
