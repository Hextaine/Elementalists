package com.hextaine.elementalists.afflictions;
/*
 * Created by Hextaine on 12/22/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import org.bukkit.Bukkit;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Wrapper class to contain both custom afflictions and base minecraft afflictions. This
 * allows the custom {@link AfflictionType}s to be integrated into the PotionEffects.
 */
public class Affliction extends PotionEffect {
    
    /**
     * Constructs a PotionEffect from a given PotionEffectType ({@link AfflictionType}
     * and will automatically turn particles off (also set ambient true and null color).
     */
    public Affliction(PotionEffectType type, int duration, int amplifier) {
        super(type, duration, amplifier);
    }
}
