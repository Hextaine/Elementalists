package com.hextaine.elementalists.afflictions.afflictiontypes;
/*
 * Created by Hextaine on 12/24/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.util.TargetCalculation;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class Grounded extends AfflictionType {
    @Override
    public double getDurationModifier() {
        return 1;
    }

    @Override
    public String getName() {
        return "Grounded";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return (Color) null;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;

        Bukkit.getPluginManager().registerEvents(this, Elementalists.pl);
    }

    public void endEffect() {
        PlayerMoveEvent.getHandlerList().unregister(this);
    }

    @EventHandler
    public void onMovementEvent(PlayerMoveEvent e) {
        if (e.getPlayer().equals(target)) {
            if (e.getTo().subtract(e.getFrom()).getY() > 0) {
                e.getTo().setY(e.getFrom().getY());
            }

            if (e.getPlayer().getVelocity().getY() > 0) {
                e.getPlayer().getVelocity().setY(0);
            }
        }
    }

    @EventHandler
    public void onTeleportListener(EntityTeleportEvent e) {
        if (e.getEntity().equals(target.getEntity())) {
            if (!e.getTo().subtract(0, 1, 0).getBlock().getType().isOccluding()) {
                e.getTo().setY(TargetCalculation.getGroundLocation(e.getTo()).getY());
            }
        }

        // e.getEntity().
    }
}
