package com.hextaine.elementalists.afflictions.afflictiontypes;

import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.spells.SpellCastEvent;
import org.bukkit.Color;
import org.bukkit.event.EventHandler;

public class Weakened extends AfflictionType {
    private double power;

    public Weakened(double power) {
        this.power = power;
    }

    @Override
    public double getDurationModifier() {
        return 0;
    }

    @Override
    public String getName() {
        return "Empowered";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return null;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;
    }

    public void endEffect() {
    }

    // @EventHandler
    // public void onSpellCast(SpellCastEvent event) {
    // if (!event.getCaster().equals(afflictedPlayer))
    // return;

    // event.setPowerModifier(power);
    // }
}
