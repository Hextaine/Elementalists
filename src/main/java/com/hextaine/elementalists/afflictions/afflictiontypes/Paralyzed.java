package com.hextaine.elementalists.afflictions.afflictiontypes;
/*
 * Created by Hextaine on 12/23/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;

import org.bukkit.Color;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

public class Paralyzed extends AfflictionType {
    @Override
    public double getDurationModifier() {
        // TODO: different duration modifiers
        return 1;
    }

    @Override
    public String getName() {
        return "Paralysis";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return (Color) null;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;
    }

    public void endEffect() {
    }

    // @EventHandler
    // public void onMoveEvent(PlayerMoveEvent e) {
    // if (e.getPlayer().equals(e.)) {

    // // if (e.getTo().getY() - e.getFrom().getY() > 0) {
    // // e.setCancelled(true);
    // // }
    // }
    // }
}
