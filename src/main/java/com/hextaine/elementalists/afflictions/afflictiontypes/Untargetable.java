package com.hextaine.elementalists.afflictions.afflictiontypes;
/*
 * Created by Hextaine on 12/28/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;

import org.bukkit.Color;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

public class Untargetable extends AfflictionType {
    @Override
    public double getDurationModifier() {
        return 1;
    }

    @Override
    public String getName() {
        return "Untargetable";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return null;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;
    }

    public void endEffect() {
    }

    // @EventHandler
    // public void stopDamageKnockback(EntityDamageEvent e) {
    // if (e.getEntityType().equals(EntityType.PLAYER)) {
    // Player playerHit = (Player)e.getEntity();
    // if (playerHit.equals(afflictedPlayer)) {
    // e.setCancelled(true);
    // ((Player) e.getEntity()).setHealth(((Player) e.getEntity()).getHealth() - e.getFinalDamage());
    // }
    // }
    // }
}
