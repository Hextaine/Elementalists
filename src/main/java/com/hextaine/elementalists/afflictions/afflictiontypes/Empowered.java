package com.hextaine.elementalists.afflictions.afflictiontypes;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.event.EventHandler;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.spells.SpellCastEvent;

public class Empowered extends AfflictionType {
    private double power;
    private double durationMod;

    public Empowered(double powerMod, double durationMod) {
        this.power = powerMod;
        this.durationMod = durationMod;
    }

    @Override
    public double getDurationModifier() {
        return durationMod;
    }

    @Override
    public String getName() {
        return "Empowered";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return Color.YELLOW;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;

        Bukkit.getPluginManager().registerEvents(this, Elementalists.pl);
    }

    public void endEffect() {
        SpellCastEvent.getHanderList().unregister(this);
    }

    @EventHandler
    public void onSpellCast(SpellCastEvent event) {
        if (!event.getCaster().equals(target))
            return;

        event.setPowerModifier(power);
        event.setDurationModifier(durationMod);
    }
}
