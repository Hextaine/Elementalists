package com.hextaine.elementalists.afflictions.afflictiontypes;
/*
 * Created by Hextaine on 12/24/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.spells.SpellCastEvent;
import org.bukkit.Color;
import org.bukkit.event.EventHandler;

public class Silenced extends AfflictionType {
    @Override
    public double getDurationModifier() {
        return 1;
    }

    @Override
    public String getName() {
        return "Silenced";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return (Color) null;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;
    }

    public void endEffect() {
    }

    // @EventHandler
    // public void onSpellCast(SpellCastEvent e) {
    // if (e.getCaster().equals(afflictedPlayer)) {
    // e.setCancelled(true);
    // }
    // }
}
