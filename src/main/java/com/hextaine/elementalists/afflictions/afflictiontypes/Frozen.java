package com.hextaine.elementalists.afflictions.afflictiontypes;
/*
 * Created by Hextaine on 12/26/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.afflictions.AfflictionType;
import com.hextaine.elementalists.entity.EntityStats;

public class Frozen extends AfflictionType {
    private int freezingTaskId;

    public Frozen(EntityStats target) {
        this.target = target;
    }

    @Override
    public double getDurationModifier() {
        return 1;
    }

    @Override
    public String getName() {
        return "Frozen";
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Color getColor() {
        return Color.AQUA;
    }

    public void applyEffect(EntityStats target) {
        this.target = target;

        final AfflictionType thisEffect = this;
        this.target.getEntity().setGravity(false);
        final Location startingLocation = this.target.getEntity().getLocation().clone();
        this.freezingTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Elementalists.pl, new Runnable() {
            public void run() {
                if (!target.hasActivePotionOfType(thisEffect)) {
                    endEffect();
                }

                if (!target.getEntity().getVelocity().isZero())
                    target.getEntity().setVelocity(new Vector(0, 0, 0));
                if (!target.getEntity().getLocation().equals(startingLocation))
                    target.getEntity().teleport(startingLocation);
            }
        }, 0L, 1L);
    }

    public void endEffect() {
        this.target.getEntity().setGravity(true);
        Bukkit.getScheduler().cancelTask(this.freezingTaskId);
    }
}
