package com.hextaine.elementalists.afflictions;
/*
 * Created by Hextaine on 12/22/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffectType;

import com.hextaine.elementalists.entity.EntityStats;

// Wrapper class to PotionEffectTypes to allow for the expansion into custom effects using the
// base Bukkit potion effect system (hopefully)
public abstract class AfflictionType extends PotionEffectType implements Listener {
    protected EntityStats target;

    // Always call super(0) because bukkit only allows 28 effects and 1-27 are taken
    // Also 0 is allowed for custom effects so ID won't be unique, but at least we can use names
    protected AfflictionType() {
        super(0, null);
    }

    public abstract void applyEffect(EntityStats target);

    public abstract void endEffect();
}
