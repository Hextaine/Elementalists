package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/11/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class DatabaseConnectionLoginListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void loginListenerDatabaseConnection(PlayerLoginEvent event) {
    }
}
