package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 2/3/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.game.GameJoinEvent;
import com.hextaine.elementalists.game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class JoinGameListener implements Listener {

    /**
     * Listens for when a player joins a game that hadn't had enough players
     * to start before the current player had joined. If the player joining
     * increases the number of players to an acceptable number, the game will
     * start it's countdown sequence to begin.
     *
     * @param event     the event called when a player joins a game
     * @see com.hextaine.elementalists.game.Game
     * @see GameState
     */
    @EventHandler
    public void checkCanStart(GameJoinEvent event) {
        if (event.getGame().getNumPlayers() >= event.getGame().getMinNumPlayers()) {
            if (!event.getGame().gameState.equals(GameState.STARTING))
                event.getGame().beginStartSequence();
        }
    }
}
