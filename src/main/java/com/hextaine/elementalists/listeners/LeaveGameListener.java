package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 2/4/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.game.Game;
import com.hextaine.elementalists.game.GameLeaveEvent;
import com.hextaine.elementalists.game.GameState;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class LeaveGameListener implements Listener {

    /**
     * Listens for specifically when a player leaves a game that hasn't started yet
     * and the number of players after this player leaves drops the number of active
     * players below the minimum to start. When this happens, cancel the game's
     * start sequence.
     *
     * @param event     the specific event that the player leaving triggers
     * @see GameState
     */
    @EventHandler
    public void cancelStartWhenLeave(GameLeaveEvent event) {
        if (event.getGame().gameState.equals(GameState.WAITING) &&
                event.getGame().getNumPlayers() <= event.getGame().getMinNumPlayers())
            event.getGame().cancelStartSequence();
    }

    /**
     * Listens for when players leave a game that is currently running, but there aren't
     * enough players to keep playing the game (usually 1 or 0 players). When this
     * happens, the game is forced to end after a small delay (10s by default).
     *
     * @param event     the event that the player leaving triggers
     * @see GameState
     */
    @EventHandler
    public void exitGameWhenNobodyIsLeft(GameLeaveEvent event) {
        if (event.getGame().gameState.equals(GameState.ACTIVE) &&
                event.getGame().getNumPlayers() <= 1) {
            event.getGame().getMessenger().sendPluginMessage("Everyone else has left the game!\n" +
                    "This game will be forced to end in 10 seconds.");

            final Game gameToStop = event.getGame();
            Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
                public void run() {
                    gameToStop.stop();
                }
            }, 200L); // NOTE: Make this not a magic number
        }
    }
}
