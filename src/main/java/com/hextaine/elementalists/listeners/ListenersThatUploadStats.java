package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/12/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.game.GameEndEvent;
import com.hextaine.elementalists.game.GameLeaveEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ListenersThatUploadStats implements Listener {

    @EventHandler
    public void onQuitDuringGame(PlayerQuitEvent event) {
        PlayerStats pStats = PlayerStats.getStats(event.getPlayer());
        if (pStats.isInGame()) {
            pStats.getCurrentGame().removePlayer(pStats);
        }
    }

    @EventHandler
    public void onGameEndUpdateStats(GameEndEvent event) {
        if (event.getGame().getPlayers().length < 2)
            return;
        
        for (PlayerStats pStats : event.getGame().getPlayers()) {
            pStats.uploadGameStats();
            pStats.resetGameStats();
        }
    }

    @EventHandler
    public void midGameLeaveEvent(GameLeaveEvent event) {
        event.getPlayer().uploadGameStats();
        event.getPlayer().resetGameStats();
    }
}
