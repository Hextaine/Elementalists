package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/19/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class KillCreditListener implements Listener {
    
    @EventHandler
    public void onKillGiveCredit(EntityDeathEvent event) {
        EntityStats eStats = null;
        if (!(event.getEntity() instanceof Player) && (eStats = EntityStats.getStats(event.getEntity())) == null)
            return;
        
        // double check just to be sure
        if (eStats == null) return;
        
        Messenger messenger = null;
        String targetKilled;
        
        if (!(event.getEntity() instanceof Player)) {
            if (event.getEntity().getCustomName() == null) {
                return;
            }
            
            targetKilled = event.getEntity().getCustomName();
            event.getEntity().setCustomName(null);
        }
        else {
            PlayerStats pstats = (PlayerStats) eStats;
            if (pstats.getCurrentGame() != null)
                messenger = pstats.getCurrentGame().getMessenger();
        }
    
        if (messenger != null && eStats.getLastDamager() != null)
            messenger.sendMessage(eStats.getLastDamager().getEntity().getName() +
                    " killed " + eStats.getEntity().getName() + " using " + eStats.getLastDamageSource());
    }
}
