package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/26/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class KeepItemsOnDeathListener implements Listener {

    @EventHandler (priority=EventPriority.MONITOR)
    public void onDeathDontDropItems(PlayerDeathEvent event) {
        if (PlayerStats.getStats(event.getEntity()).getCurrentGame() != null) {
            event.setKeepInventory(true);
        }
    }
}
