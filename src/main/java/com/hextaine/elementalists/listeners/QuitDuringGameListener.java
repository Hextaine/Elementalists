package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/26/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitDuringGameListener implements Listener {

    @EventHandler
    public void onQuitLeaveGame(PlayerQuitEvent event) {
        PlayerStats pStats = null;
        if ((pStats = PlayerStats.getStats(event.getPlayer())).getCurrentGame() != null) {
            pStats.getCurrentGame().removePlayer(pStats);
        }
    }
}
