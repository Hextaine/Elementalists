package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/29/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockChangeInArenaListener implements Listener {

    @EventHandler(
            priority = EventPriority.HIGH
    )
    public void onBlockBreakEvent(BlockBreakEvent event) {
        if (event.getPlayer() != null) {
            if (PlayerStats.getStats(event.getPlayer()).isInGame()) {
                event.setCancelled(true);
            }
        }

        // TODO: Possible delayed reset
        if (event.getBlock().hasMetadata("arena"))
            event.setCancelled(true);
    }
}
