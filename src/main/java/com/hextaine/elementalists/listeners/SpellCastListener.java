package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 1/3/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.SpellCastEvent;
import com.hextaine.elementalists.spells.Spells;

public final class SpellCastListener implements Listener {
    @EventHandler
    public void onAttemptedSpellCast(PlayerInteractEvent event) {
        ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
        if (inHand == null || inHand.getItemMeta() == null || !inHand.getItemMeta().hasLore())
            return;
        if (!inHand.getItemMeta().hasDisplayName())
            return;

        Spells spell = Spells.getSpellByName(ChatColor.stripColor(inHand.getItemMeta().getDisplayName()));
        if (spell == null)
            return;

        PlayerStats caster = Elementalists.playerStats.get(event.getPlayer());
        if (caster == null) {
            caster = PlayerStats.createStats(event.getPlayer());
            Elementalists.playerStats.put(event.getPlayer(), caster);
        }

        SpellCastEvent spellCastEvent = new SpellCastEvent(spell.getSpellHandler(), caster);
        Bukkit.getServer().getPluginManager().callEvent(spellCastEvent);

        if (spellCastEvent.isCancelled()) {
            return;
        }

        spell.cast(caster, spellCastEvent.getModifier(), true);
    }
}
