package com.hextaine.elementalists.listeners;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class MessengerListeners implements Listener {
    
    @EventHandler
    public void onJoinMessangerAdd(PlayerJoinEvent e) {
        // TODO if e.DoesNotWantToIgnore
        Elementalists.generalMessenger.add(e.getPlayer());
        
        if (e.getPlayer().hasPermission("elementalists.staff")) {
            Elementalists.staffMessenger.add(e.getPlayer());
        }
        
        if (e.getPlayer().hasPermission("elementalists.dev")) {
            Elementalists.devMessenger.add(e.getPlayer());
        }
    }
    
    @EventHandler
    public void onQuiteMessangerRemove(PlayerQuitEvent e) {
        if (Elementalists.generalMessenger.contains(e.getPlayer()))
            Elementalists.generalMessenger.add(e.getPlayer());
    }
}
