package com.hextaine.elementalists;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.hextaine.elementalists.commands.Cast;
import com.hextaine.elementalists.commands.ElementalistsCmd;
import com.hextaine.elementalists.commands.Register;
import com.hextaine.elementalists.commands.SpellBind;
import com.hextaine.elementalists.database.AbstractDbHandler;
import com.hextaine.elementalists.database.SqlLiteDb;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.game.Arena;
import com.hextaine.elementalists.listeners.SpellCastListener;
import com.hextaine.elementalists.spells.Spells;
import com.hextaine.elementalists.util.LogHandler;
import com.hextaine.elementalists.util.Messenger;
import com.hextaine.elementalists.util.config.ArenaConfig;
import com.hextaine.elementalists.util.config.GameConfig;
import com.hextaine.elementalists.util.config.InventoryConfig;

public class Elementalists extends JavaPlugin {
    public static Plugin pl;
    public static LogHandler log;
    public static Messenger generalMessenger;
    public static Messenger staffMessenger;
    public static Messenger devMessenger;
    public static AbstractDbHandler dbManager;
    public static WeakHashMap<Player, PlayerStats> playerStats;
    private static ArenaConfig arenaConfig;
    private static GameConfig gameConfig;

    private static InventoryConfig invConfig; // TODO make Inventory configs

    /**
     * The standard enabling method that should instantiate all runtime objects such
     * as the Messengers, the PlayerStats, any configs, and create and/or connect to
     * the database.
     */
    @Override
    public void onEnable() {
        pl = this;
        log = new LogHandler(this);

        generalMessenger = new Messenger();
        staffMessenger = new Messenger();
        devMessenger = new Messenger();

        Spells.initialize();

        playerStats = new WeakHashMap<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            playerStats.put(player, PlayerStats.createStats(player));
        }

        if (!this.getDataFolder().exists())
            if (!this.getDataFolder().mkdir())
                log.logError("Failed to create data folder for " + pl.getName() + "!");

        dbManager = new SqlLiteDb();
        try {
            dbManager.connectToDB();
        } catch (Exception e) {
            Bukkit.getServer().getLogger()
                    .severe("Failed to open connection to DB manager:\n" + e.getStackTrace().toString());
        }

        // Initialize the class properly so we can reference the static variables later on even if we don't define an arena
        new Arena("", null);

        arenaConfig = new ArenaConfig("arenas");
        gameConfig = new GameConfig("games");

        arenaConfig.load();
        gameConfig.load();

        // Prepare Command Functions
        getCommand("cast").setExecutor(new Cast());
        getCommand("register").setExecutor(new Register());
        getCommand("spellbind").setExecutor(new SpellBind());
        getCommand("elementalists").setExecutor(new ElementalistsCmd());

        this.getServer().getPluginManager().registerEvents(new SpellCastListener(), this);

        log.logMessage("Elementalists fully loaded.");
    }

    /**
     * Disables all runtime objects in a safe, controled manner by uploading any
     * remaining
     * database variables and saving the configs.
     */
    @Override
    public void onDisable() {
        for (PlayerStats playerStats : playerStats.values()) {
            playerStats.uploadGameStats();
        }

        arenaConfig.save();
        gameConfig.save();

        log.logMessage("Elementalists safely shut down.");
    }
}
