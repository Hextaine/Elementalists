package com.hextaine.elementalists.commands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.commands.subcommands.ArenaCmd;
import com.hextaine.elementalists.commands.subcommands.GameCmd;
import com.hextaine.elementalists.commands.subcommands.GuiCmd;
import com.hextaine.elementalists.commands.subcommands.Help;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ElementalistsCmd implements CommandExecutor {
    
    /**
     * General command usage: /elementalists (alias: /e). Uses of the command include
     * control over the creation of arenas / games, flow of a game, joining and leaving games,
     * adding arenas to games, reloading the configs, deleting games or arenas, bringing
     * up GUIs, and a few other debugging features.
     *
     * @param sender    whoever used the command
     * @param cmd       the command represented wholly as an object
     * @param cmdLabel  the command name string
     * @param args      the arguements of the command
     * @return  <code>true</code> if the command was properly handled or <code>false</code>
     *          if it wasn't able to be handled.
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
        // Disable players without enough permissions to access any of the following permissions
        if (!sender.hasPermission("elementalists.admin")) {
            Messenger.sendErrorMessage(sender, "Sorry, but you don't have enough permissions to use this command.");
            return false;
        }
    
        if (cmdLabel.equalsIgnoreCase("elementalists")) {
            // Check if the sender is asking or needs the help menu
            if (args.length == 0 || args[0].equalsIgnoreCase("help") ||
                    args[0].equalsIgnoreCase("h")) {
                Help.handle(sender, 0);
                return true;
            }
        
            // GUI Control Commands
            if (args[0].equalsIgnoreCase("gui")) {
                GuiCmd.handle(sender, args);
                return true;
            }
        
            // Arena Manipulation Commands.
            if (args[0].equalsIgnoreCase("arena")) {
                ArenaCmd.handle(sender, args);
                return true;
            }
        
            // Game Manipulation Commands.
            if (args[0].equalsIgnoreCase("game")) {
                GameCmd.handle(sender, args);
                return true;
            }
        }
    
        return false;
    }
}
