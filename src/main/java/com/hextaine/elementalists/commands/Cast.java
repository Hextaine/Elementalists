package com.hextaine.elementalists.commands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.SpellModifier;
import com.hextaine.elementalists.spells.Spells;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cast implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String cmdLabel, String[] args) {
        if (cmdLabel.equalsIgnoreCase("cast")) {
            if (!(sender instanceof Player)) {
                Messenger.sendErrorMessage(sender, "Sorry, only players can use this set of commands!");
                return true;
            }
        
            if (args.length == 0) {
                Messenger.sendErrorMessage(sender, "You must specify a spell!");
                return true;
            }
        
            Spells spell = Spells.getSpellByName(args[0]);
        
            if (spell == null) {
                Messenger.sendErrorMessage(sender, "Sorry, the spell, " + args[0] + ", could not be found!");
                return true;
            }
        
            PlayerStats caster = Elementalists.playerStats.get(sender);
            if (caster == null) {
                Messenger.sendErrorMessage(sender, ChatColor.DARK_PURPLE + "Sorry, but since you are not registered with the Elementum, you are " +
                        "not allowed to cast spells. Try registering first!");
                return true;
            }
        
            if (args.length == 1) {
                Messenger.sendPluginMessage(sender, "Casting " + spell.getSpellHandler().getName() + "...");
                spell.getSpellHandler().cast(caster, SpellModifier.REGULAR);
                return true;
            }
        
            try {
                switch (Integer.parseInt(args[1])) {
                    case 0:
                        spell.getSpellHandler().cast(caster, SpellModifier.REGULAR);
                        break;
                    case 1:
                        spell.getSpellHandler().cast(caster, SpellModifier.EMPOWERED);
                        break;
                    case 2:
                        spell.getSpellHandler().cast(caster, SpellModifier.WEAKENED);
                        break;
                    case 3:
                        spell.getSpellHandler().cast(caster, SpellModifier.LENGTHEN);
                        break;
                    case 4:
                        spell.getSpellHandler().cast(caster, SpellModifier.SHORTEN);
                        break;
                    default:
                        spell.getSpellHandler().cast(caster, SpellModifier.REGULAR);
                        break;
                }
            
                Messenger.sendPluginMessage(sender, "Casting " + spell.getSpellHandler().getName() + "...");
            } catch (Exception e) {
                Messenger.sendErrorMessage(sender, "Invalid option specified: " + args[1] + ". Please use a number.");
            }
        
            return true;
        }
    
        return false;
    }
}
