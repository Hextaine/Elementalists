package com.hextaine.elementalists.commands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Register implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
        if (cmdLabel.equalsIgnoreCase("register")) {
            if (args.length != 1) {
                Messenger.sendErrorMessage(sender, "Please specify a player to register.");
                return true;
            }
        
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                if (onlinePlayer.getName().equalsIgnoreCase(args[0])) {
                    if (Elementalists.playerStats.containsKey(onlinePlayer)) {
                        Messenger.sendErrorMessage(sender, ChatColor.DARK_RED + "That player is already registered! " +
                                "No need to do it a second time.");
                        return true;
                    }
                    else {
                        Elementalists.playerStats.put(onlinePlayer, PlayerStats.createStats(onlinePlayer));
                        Messenger.sendPluginMessage(sender, ChatColor.DARK_AQUA + "Player successfully registered.");
                        return true;
                    }
                }
            }
    
            Messenger.sendErrorMessage(sender, ChatColor.DARK_RED + "Could not find the player, " + args[0] + ", among the online players.");
        }
        return false;
    }
}
