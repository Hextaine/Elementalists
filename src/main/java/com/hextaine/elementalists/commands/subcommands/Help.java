package com.hextaine.elementalists.commands.subcommands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Help {
    
    /**
     * Display a help menu to the user of the command given the level of help which you wish
     * to see. Each level represents a different set of commands: general elementalists help menu,
     * the help menu for arena commands, or the help menu for game commands.
     *
     * @param helpOption    the level of help which you wish to see (WIP)
     * @param sender        whoever used the command
     */
    public static void handle(CommandSender sender, int helpOption) {
            sender.sendMessage(ChatColor.DARK_GREEN + "\n\t|===| Elementalists |===|\n" +
                    "/elementalists -- diaply this help menu\n" +
                    "/elementalists help -- displayer this help menu\n" +
                    "/elementalists arena -- Arena configuration command base");
        }
}
