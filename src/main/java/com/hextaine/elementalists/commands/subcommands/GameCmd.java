package com.hextaine.elementalists.commands.subcommands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.game.Game;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameCmd {
    
    public static void handle(CommandSender sender, String[] args) {
    
        // Display the help menu for Game commands if asked or if no other arguments are provided.
        if (args.length == 1 || args[1].equalsIgnoreCase("help") || args[1].equalsIgnoreCase("h")) {
            Help.handle(sender, 2);
            return;
        }
    
        // Create a Game.
        if (args[1].equalsIgnoreCase("create")) {
            // Make sure we have the appropriate number of arguments.
            if (args.length != 3) {
                Messenger.sendErrorMessage(sender, "Invalid format of command!\n" +
                        "Usage: /elementalists game create <name>");
                return;
            }
        
            // Actually create the game (no need to save it anywhere, it is automatically added to Game.games).
            new Game(args[2]);
            return;
        }
    
        // Restrict non-player CommandSenders beyond this point due to the use of Player Locations
        if (!(sender instanceof Player)) {
            sender.sendMessage("Sorry, that command can only be used by a player");
            return;
        }
    
        Player player = (Player)sender;
    
        // Set the Lobby to the player's current location.
        if (args[1].equalsIgnoreCase("setLobby")) {
            // Make sure we have the appropriate number of arguments.
            if (args.length != 3) {
                Messenger.sendErrorMessage(player, "Invalid format of command!\n" +
                        "Usage: /elementalists game setLobby <name>");
                return;
            }
        
            // Check if the provided ID for the game is valid
            if (!Game.gameExists(args[2])) {
                Messenger.sendErrorMessage(player, "Could not find a game with identifier, " + args[3] + ".\n");
                return;
            }
        
            // Set the game's lobby location to the player's current location
            Game.getGame(args[2]).setLobby(player.getLocation());
            Messenger.sendPluginMessage(sender, "Game, " + args[2] + "'s Lobby was set to your location!");
        
            return;
        }
    }
}
