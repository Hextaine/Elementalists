package com.hextaine.elementalists.commands.subcommands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.util.InventoryGui;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class GuiCmd {
    
    public static void handle(CommandSender sender, String[] args) {
        // If the player only inputs the gui subcommand without specifying action
        if (args.length == 1) {
            sender.sendMessage(ChatColor.DARK_RED + "/elementalists gui create|edit|open|delete");
            return;
        }
        
        if (InventoryGui.activeInventories == null)
            InventoryGui.activeInventories = new HashMap<>();
    
        switch (args[1].toLowerCase()) {
            // Create an inventory gui
            case "create":
                // Must have 4 arguments: gui subcommand, create action, slot number, name of gui.
                if (args.length != 4) {
                    Messenger.sendErrorMessage(sender, "/elementalists gui create <slots> <name>");
                    return;
                }
                
                // Cancel the command if an inventory with that title already exists.
                if (InventoryGui.activeInventories != null
                        && InventoryGui.activeInventories.containsKey(args[3])) {
                    Messenger.sendErrorMessage(sender, "That inventory already exists!");
                    return;
                }
            
                int size = 0;
                
                // Try to format the number of slots into an integer and create the GUI
                try {
                    size = Integer.parseInt(args[2]);
                } catch (NumberFormatException nfe) {
                    Messenger.sendErrorMessage(sender, "<slots> must be a number, not " + args[2] + "!");
                    return;
                }
    
                if (size <= 9 && size % 9 != 0) {
                    Messenger.sendErrorMessage(sender, "Sorry, but the size of the inventory must be a positive " +
                            "multple of 9.");
                    return;
                }
                
                new InventoryGui(Integer.parseInt(args[2]), args[3]);
                Messenger.sendPluginMessage(sender, "Created InventoryGui, " + args[3] + "!");
                break;
        
            // Edit an already existent GUI
            case "edit":
                // Make sure the console isn't requesting to edit the inventory
                if (!(sender instanceof Player)) {
                    Messenger.sendErrorMessage(sender, "You must be a player to edit inventories!");
                    return;
                }
            
                // There must be 3 arguments: gui subcommand, edit action, name of gui to edit
                // (must already exist).
                if (args.length != 3) {
                    Messenger.sendErrorMessage(sender, "/elementalists gui edit <name>");
                    return;
                }
            
                // Check that the GUI exists
                if (!InventoryGui.activeInventories.containsKey(args[2])) {
                   Messenger.sendErrorMessage(sender, "Could not find any InventoryGUI with the name, " + args[2] + "!");
                    return;
                }
            
                // Open the Edit GUI for the player that sent the command.
                InventoryGui.activeInventories.get(args[2]).openEditGui((Player) sender);
            
                break;
        
            // Open an already existent GUI.
            case "open":
                // Make sure the console isn't requesting to open a GUI.
                if (!(sender instanceof Player)) {
                    Messenger.sendErrorMessage(sender, "You must be a player to open inventories!");
                    return;
                }
            
                // Verify the command has 3 arguments: gui subcommand, open action, and the name of the
                // gui to open (must already exist).
                if (args.length != 3) {
                    Messenger.sendErrorMessage(sender, "/elementalists gui open <name>");
                    return;
                }
            
                // Verify the GUI with the given name exists.
                if (InventoryGui.activeInventories == null ||
                        !InventoryGui.activeInventories.containsKey(args[2])) {
                    Messenger.sendErrorMessage(sender, "Could not find any InventoryGUI with the name, " + args[2] + "!");
                    return;
                }
                
                // Open the GUI for the player that sent the command.
                InventoryGui.activeInventories.get(args[2]).show((Player) sender);
            
                break;
        
            // Delete an already existent gui.
            case "delete":
                // Ensure there are 3 arguments: gui subcommand, delete action, and the name of the GUI
                // being deleted (must already exist).
                if (args.length != 3) {
                    Messenger.sendErrorMessage(sender, "/elementalists gui delete <name>");
                    return;
                }
            
                // Verify the InventoryGui actually exists.
                if (!InventoryGui.activeInventories.containsKey(args[2])) {
                    Messenger.sendErrorMessage(sender, "Could not find any InventoryGUI with the name, " + args[2] + "!");
                    return;
                }
            
                // Delete the InventoryGui.
                InventoryGui.deleteInventory(args[2]);
            
                Messenger.sendPluginMessage(sender, "Deleted inventory, '" + args[2] + "'!");
                
                break;
                
            case "set":
                if (args.length < 4) {
                    Messenger.sendErrorMessage(sender, "Please specify the inventory you wish to change a " +
                            "property of as well as the property you wish to toggle!");
                    return;
                }
                
                if (!InventoryGui.activeInventories.containsKey(args[2])) {
                    Messenger.sendErrorMessage(sender, "Cannot find the GUI named, '" + args[2] + "'.");
                    return;
                }
                
                switch (args[3].toLowerCase()) {
                    case "dispense":
                        if (args.length != 5) {
                            Messenger.sendErrorMessage(sender, "/elementalists gui set " + args[2]
                                    + " dispense <true|false>");
                            return;
                        }
                        
                        boolean dispense = false;
                        
                        try {
                            dispense = Boolean.parseBoolean(args[4]);
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                        
                        InventoryGui.activeInventories.get(args[2]).setDispensor(dispense);
                        Messenger.sendPluginMessage(sender, "Successfully set Dispense to "
                                + ChatColor.DARK_GREEN + dispense);
                        
                        return;
                }
                
            default:
                Messenger.sendErrorMessage(sender, "/elementalists gui create|edit|set|open|delete");
        }
    }
}
