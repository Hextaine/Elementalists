package com.hextaine.elementalists.commands.subcommands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.game.Game;
import com.hextaine.elementalists.util.Messenger;
import org.apache.commons.lang3.tuple.MutablePair; // TODO Remove Mutable pair dependency
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ArenaCmd {
    private static HashMap<Player, MutablePair<Location, Location>> arenaPos = new HashMap<>();
    
    public static void handle(CommandSender sender, String[] args) {
        // If only '/elementalists area' is provided or the help menu is specified, display
        // the help menu.
        if (args.length == 1 || args[1].equalsIgnoreCase("help") || args[1].equalsIgnoreCase("h")) {
            // TODO
            Help.handle(sender, 1);
            return;
        }
    
        // Require all further commands to be executed by a player (following commands use player's location.
        if (!(sender instanceof Player)) {
            Messenger.sendErrorMessage(sender, "Sorry, that command can only be used by a player");
            return;
        }
    
        Player player = (Player)sender;
    
        // Set the 1st location for an arena.
        if (args[1].equalsIgnoreCase("p1")) {
            // Insert the player into the location tracker if they aren't already there.
            if (!arenaPos.containsKey(player))
                arenaPos.put(player, new MutablePair<Location, Location>());
        
            // Set the 1st location to the player's current location.
            arenaPos.get(player).setLeft(player.getLocation());
        
            Messenger.sendPluginMessage(player, "Location 1 has been set!");
            return;
        }
    
        // Set the 2nd location for an arena.
        if (args[1].equalsIgnoreCase("p2")) {
            // Insert the player into the location tracker if they aren't already there.
            if (!arenaPos.containsKey(player))
                arenaPos.put(player, new MutablePair<Location, Location>());
        
            // Set the 2nd location to the player's current location.
            arenaPos.get(player).setRight(player.getLocation());
        
            Messenger.sendPluginMessage(player, "Location 2 has been set!");
            return;
        }
    
        // Create the Arena based off of the 2 locations previously set by '/e arena p1' and '/e arena p2'.
        // Optionally, add the arena to an existing Game if the ID provided is valid.
        if (args[1].equalsIgnoreCase("create")) {
            // Make sure we are in the sweet spot for arguments
            if (args.length < 3 || args.length > 4) {
                Messenger.sendErrorMessage(player, "Invalid format of command!\n" +
                        "Usage: /elementalists arena create <name>");
                return;
            }
        
            // If the player hasn't set both locations already, fail out and notify the player.
            if (!arenaPos.containsKey(player) ||
                    arenaPos.get(player).getLeft() == null || arenaPos.get(player).getRight() == null) {
                Messenger.sendErrorMessage(player, "You haven't set any coordinates for the area yet!\n" +
                        "Use: /elementalists arena p1 && /elementalists arena p2");
                return;
            }
        
            // Create the arena based on the two locations already set.
            com.hextaine.elementalists.game.Arena arena = new com.hextaine.elementalists.game.Arena(args[2], arenaPos.get(player).getLeft(), arenaPos.get(player).getRight());
        
            // If the 4th argument is provided and it's a valid Game ID, then add it to the game.
            if (args.length == 4) {
                if (com.hextaine.elementalists.game.Game.gameExists(args[3])) {
                    Game.getGame(args[3]).addArena(arena);
                } else {
                    Messenger.sendErrorMessage(player, "Could not find a game with identifier, " + args[3] + ".\n" +
                            "Defaulting to only creating the arena...");
                }
            }
        
            Messenger.sendPluginMessage(player, "Arena created with identifier, " + args[2]);
            return;
        }
    
        // If none of the above commands were given, assume an invalid command.
        Messenger.sendErrorMessage(sender, "Invalid Command\n" +
                "Use '/elementalists arena help' to see valid commands.");
        return;
    }
}
