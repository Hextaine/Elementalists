package com.hextaine.elementalists.commands;
/*
 * Created by Hextaine on 6/22/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.spells.Spells;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpellBind implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
        if (cmdLabel.equalsIgnoreCase("spellbind")) {
            if (!(sender instanceof Player)) {
                Messenger.sendErrorMessage(sender, "Sorry, only players can use this set of commands!");
                return true;
            }
            if (args.length == 0) {
                Messenger.sendErrorMessage(sender, "You must specify a spell!");
                return true;
            }
            Spells spell = Spells.getSpellByName(String.join(" ", args));
            if (spell == null) {
                Messenger.sendErrorMessage(sender, "Sorry, the spell, " + args[0] + ", could not be found!");
                return true;
            }

            Player player = (Player) sender;

            if (player.getInventory().getItemInMainHand() == null ||
                    player.getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
                Messenger.sendErrorMessage(sender, "You must be holding an item to bind the spell to!");
                return true;
            }

            Spells.flavorItem(player.getInventory().getItemInMainHand(), spell);
        }

        return false;
    }
}
