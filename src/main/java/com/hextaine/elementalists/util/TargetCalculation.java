package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/23/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.entity.EntityStats;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.spells.Spell;

public class TargetCalculation {
    public static final int MAXRANGE = 100;
    private static final float TOLERANCE = 0.5f;

    /**
     * rayCastSingleTarget uses a direction vector to search for entities (players) along the line
     * a player is looking along until the max range. The first target found (that isn't the caster)
     * is returned.
     *
     * @param caster
     *            the player from which the raycast is sent
     * @param range
     *            the range at which you wish to max out the raycast
     * @return the entity ({@link EntityStats} the player is looking at or <code>null</code> if
     *         an entity cannot be located along where the player is looking
     */
    public static EntityStats rayCastSingleTarget(EntityStats caster, int range) {
        Vector direction = caster.getEntity().getLocation().getDirection();
        Location loc = caster.getEntity().getLocation().add(0, 1, 0);
        double distance = 0.0;

        Collection<Entity> nearEnts = null;
        // Move the location forward so we don't accidentally detect people behind the caster
        loc = loc.add(direction);
        // Loop until the distance has been maxed out
        while (distance < range * range) {
            // If the block is not solid along the vector line, then we don't want the cast to continue
            if (loc.getBlock().getType().isSolid())
                break;

            // Make sure the location we are checking is still withing the vertical bounds of the Minecraft world
            if (loc.getBlockY() >= 256 || loc.getBlockY() <= 0)
                break;

            // Get all entities near the current iterated location along the vector
            nearEnts = loc.getWorld().getNearbyEntities(loc, TOLERANCE, TOLERANCE, TOLERANCE);
            // If there are nearby entities
            if (nearEnts != null && nearEnts.size() > 0) {
                for (Entity e : nearEnts) {
                    EntityStats stats = EntityStats.getStats(e);
                    // if the nearby entities are able to be hit by the spell
                    if (stats != null) {
                        // If the target isn't the caster, then return
                        if (!stats.equals(caster))
                            return stats.resolveTarget();
                    }
                }
            }

            // Progress the location forwards along the vector
            loc = loc.add(direction);
            distance = loc.distanceSquared(caster.getEntity().getLocation());
        }

        return null;
    }

    /**
     * Gets all targets along the vector the player is looking until the default MAXRANGE. Mainly
     * a convenience method to call rayCastAllTarget given the caster at max range.
     *
     * @param caster
     *            the caster the ray cast will be sent from
     * @return all entities in EntityStats array that collide with the location direction
     */
    public static EntityStats[] rayCastAllTarget(Player caster) {
        return rayCastAllTarget(caster, MAXRANGE);
    }

    /**
     * Gets all entities in line of a small episilon range from where the caster (player) is looking
     * with a provided max range.
     *
     * @param caster
     *            the caster the raycast extends from
     * @param range
     *            the maximum range the raycast will check for entities
     * @return All entities within an epsilon that are along the sight-path of the caster
     */
    // rayCastAllTarget will get all possible targets along the vector the player is looking given the new
    // range.
    public static EntityStats[] rayCastAllTarget(Player caster, int range) {
        ArrayList<EntityStats> targets = new ArrayList<>();
        Vector direction = caster.getLocation().getDirection();
        Location loc = caster.getEyeLocation();
        double distance = 0.0;

        Collection<Entity> nearEnts = null;
        // Move the location forward so we don't accidentally detect people behind the caster
        loc = loc.add(direction);
        // Loop until the distance has been maxed out
        while (distance < range * range) {
            // If the block is not solid along the vector line, then we don't want the cast to continue
            if (loc.getBlock().getType().isSolid())
                break;

            // Make sure the location we are checking is still withing the vertical bounds of the Minecraft world
            if (loc.getBlockY() >= 256 || loc.getBlockY() <= 0)
                break;

            // Get all entities near the current iterated location along the vector
            nearEnts = loc.getWorld().getNearbyEntities(loc, TOLERANCE, TOLERANCE, TOLERANCE);
            // If there are nearby entities
            if (nearEnts != null && nearEnts.size() > 0) {
                for (Entity e : nearEnts) {
                    EntityStats stats = EntityStats.getStats(e);
                    // if the nearby entities are able to be hit by the spell
                    if (stats != null) {
                        // If the target isn't the caster, then return
                        if (!stats.equals(EntityStats.getStats(caster)))
                            targets.add(stats);
                    }
                }
            }

            // Progress the location forwards along the vector
            loc = loc.add(direction);
            distance = loc.distanceSquared(caster.getLocation());
        }

        EntityStats[] allTargets = new EntityStats[targets.size()];
        return targets.toArray(allTargets);
    }

    /**
     * Gets a normalized vector that points from the 'from' location to the 'to' location.
     *
     * @param from
     *            starting location
     * @param to
     *            direction the vector should point
     * @return the normalized vector (magnitude of 1) of the vector drawn from the 'from'
     *         Location to the 'to' Location
     */
    public static Vector getDirectionalVector(Location from, Location to) {
        return to.subtract(from).toVector().normalize();
    }

    /**
     * Gets the last non-solid block before reaching either a solid block (the ground) or the void.
     *
     * @param location
     *            the location at which the ground is to be located below
     * @return the non-solid block above the ground location
     */
    public static Location getGroundLocation(Location location) {
        // TODO seek above if underground
        // First clone the location so we can mess with the copy without affecting the original location
        Location loc = location.clone();

        // Iterate through all non-solid blocks below the player until a solid block is found or you reach the void
        while (!loc.getBlock().getType().isSolid() && loc.getY() > 0) {
            loc.subtract(0, 1, 0);
        }

        // Once you have the location of a solid block, reset the location to be based around the zeroed, defaulted
        // location of the block, then return its location raised by 1.
        return loc.getBlock().getLocation().getBlock().getLocation().add(0, 1, 0);
    }

    /**
     * Gets the distance from the player to the ground squared. Squared because, although small,
     * it saves a bit of computational time due to the nature of square-root.
     *
     * @param caster
     *            the caster at which we are finding the distance to the ground beneath
     * @return the distance to the ground beneath the player squared
     */
    public static double distanceSquaredToGround(Player caster) {
        Location ground = getGroundLocation(caster.getLocation());

        // First zero the location by getting the block, then getting the block's location. Then return the distance
        // squared from the player's current location.
        return ground.subtract(0, 1, 0).distanceSquared(caster.getLocation());
    }

    /**
     * Casts a vector until a solid block or the void is reached then returns the block at that location.
     *
     * @param caster
     *            the caster from which we ray-cast and get the block he/she/it is looking at
     * @param range
     *            the maximum range from which the target block can be
     * @return the first solid block the player is looking at or the last non-solid block
     *         at the max range
     */
    public static Block getTargetBlock(Player caster, int range) {
        Location loc = caster.getEyeLocation();
        Vector direction = loc.getDirection();

        // Iterate through all non-solid blocks below the player until a solid block is found, or you reach the void
        // I use > 1 and < 255 because the direction vector will add once past the while check and I don't want to get
        // an invalid block past the top most block in the sky or below the void.
        while (!loc.getBlock().getType().isSolid() &&
                loc.distanceSquared(caster.getLocation()) < range * range && loc.getY() > 1 && loc.getY() < 254) {
            loc.add(direction);
        }

        // Once you have the location of a solid block, return the block itself
        return loc.getBlock();
    }

    public static EntityStats[] getCollidingEntities(PlayerStats caster) {
        return getNearbyTargets(caster, Spell.PROJECTILETOLERANCE, false);
    }

    /**
     * Returns an array of the nearby entities (EntityStats[]) for entities within a specified,
     * cubical (getNearbyEntities) range of the player provided.
     *
     * @param caster
     *            the player from which the bounding box eminates
     * @param range
     *            the maximum range that entities will be detected
     * @return all entities (EntityStats[]) that have or can have EntityStats within the max
     *         range of the player
     */
    public static EntityStats[] getNearbyTargets(PlayerStats caster, double range, boolean allowSelf) {
        // First create a bag to throw all of the entity's that are viable into
        ArrayList<EntityStats> entityStats = new ArrayList<>();

        // Iterate through all possible nearby entities
        for (Entity e : caster.getEntity().getNearbyEntities(range, range, range)) {
            if (!allowSelf && e == caster.getEntity()) {
                continue;
            }

            EntityStats entity = EntityStats.getStats(e);
            // If the entities that are in the immediate vicinity can't be targeted by spell, skip
            if (entity == null)
                continue;

            // If the entity can be targeted, add its EntityStats to the array
            entityStats.add(entity);
        }

        // Compress all of the entities into a regular array for usability and return it
        EntityStats[] targets = new EntityStats[entityStats.size()];
        return entityStats.toArray(targets);
    }
}
