package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/19/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Messenger {
    private static final String baseFormat = ChatColor.GOLD.toString() + ChatColor.ITALIC.toString();
    private static final String officalFormat = ChatColor.DARK_AQUA.toString() + ChatColor.BOLD.toString();
    private static final String errorFormat = ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + ChatColor.ITALIC;
    private ArrayList<Player> recipients;
    
    public Messenger() {
        recipients = new ArrayList<Player>();
    }
    
    public void add(Player player) {
        recipients.add(player);
    }
    
    public void remove(Player player) {
        recipients.remove(player);
    }
    
    public boolean contains(Player player) {
        return recipients.contains(player);
    }
    
    public void sendPluginMessage(String msg) {
        for (Player recipient : recipients) {
            recipient.sendMessage(officalFormat + msg);
        }
    }
    
    public static void sendErrorMessage(CommandSender recipient, String msg) {
        recipient.sendMessage("\n" + errorFormat + msg);
    }
    
    public static void sendErrorMessage(Player[] recipients, String msg) {
        for (Player recipient : recipients) {
            recipient.sendMessage(errorFormat + msg);
        }
    }
    
    public static void sendPluginMessage(CommandSender recipient, String msg) {
        recipient.sendMessage(officalFormat + msg);
    }
    
    public void sendMessage(String message) {
        for (Player recipient : recipients) {
            recipient.sendMessage(baseFormat + message);
        }
    }
}
