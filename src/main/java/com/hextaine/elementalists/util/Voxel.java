package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.Serializable;

/**
 * An object abstraction of a 3D area (Volume! Yay Geometry) used to
 * represent the volume of an arena.
 */
public abstract class Voxel implements Serializable {

    public abstract int getMaxY();

    public abstract int getMinY();

    public abstract boolean isOutside(Location l);

    /**
     * Builds a Voxel from a given string if possible. Otherwise, returns <code>null</code>.
     *
     * @param rawVoxel
     *            the formatted containing all the information to rebuild a Voxel
     * @return a complete Voxel with from the given measures when a properly formatted
     *         string is given. Otherwise returns <code>null</code>
     * @see VoxelBox
     * @see VoxelCylinder
     */
    public static Voxel fromString(String rawVoxel) {
        String[] attributes = rawVoxel.split(":");
        String[] worldXYZ = attributes[1].split(",");
        if (attributes[0].equals("box")) {
            try {
                Location min = new Location(Bukkit.getWorld(worldXYZ[0]), Double.parseDouble(worldXYZ[1]),
                        Double.parseDouble(worldXYZ[2]), Double.parseDouble(worldXYZ[3]));

                String[] maxXYZ = attributes[2].split(",");
                Location max = new Location(min.getWorld(), Double.parseDouble(maxXYZ[0]),
                        Double.parseDouble(maxXYZ[1]), Double.parseDouble(maxXYZ[2]));

                return new VoxelBox(min, max);
            } catch (Exception e) {
                Elementalists.log.logError("Failed to parse VoxelBox: " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        } else if (attributes[0].equals("cylinder")) {
            try {
                Location center = new Location(Bukkit.getWorld(worldXYZ[0]), Double.parseDouble(worldXYZ[1]),
                        Double.parseDouble(worldXYZ[2]), Double.parseDouble(worldXYZ[3]));
                int radius = Integer.parseInt(attributes[2]);
                int height = Integer.parseInt(attributes[3]);

                return new VoxelCylinder(radius, center, height);
            } catch (Exception e) {
                Elementalists.log.logError("Failed to parse VoxelCylinder: " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        Elementalists.log.logError("Failed to categorize Voxel.");
        return null;
    }
}
