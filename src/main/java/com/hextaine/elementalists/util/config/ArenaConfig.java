package com.hextaine.elementalists.util.config;
/*
 * Created by Hextaine on 2/2/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.game.Arena;

/**
 * The implementation of the {@link ConfigFile} to save Arenas.
 */
public class ArenaConfig extends ConfigFile {
    public ArenaConfig(String path) {
        super(path);
    }

    /**
     * Loads the arenas from within the config file to the Arenas ArrayList.
     */
    @Override
    public void load() {
        for (String arena : getConfig().getStringList("arenas")) {
            Arena.arenas.add(Arena.deserialize(arena));
        }
    }

    /**
     * Saves all arenas from within the Arena ArrayList into the config file.
     */
    @Override
    public void save() {
        if (Arena.arenas != null && Arena.arenas.size() > 0) {
            String[] serializedArenas = new String[Arena.arenas.size()];
            for (int i = 0; i < serializedArenas.length; i++) {
                serializedArenas[i] = Arena.arenas.get(i).toString();
            }
    
            getConfig().set("arenas", serializedArenas);

            for (Arena arena : Arena.arenas)
                getConfig().set("spawns." + arena.getId(), arena.getSpawns());
        }

        super.save();
    }
}
