package com.hextaine.elementalists.util.config;
/*
 * Created by Hextaine on 2/2/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public abstract class ConfigFile {
    private File file;
    private YamlConfiguration yaml;
    
    /**
     * Constructor to load a file from a path that begins in the plugin's data folder.
     * Example: path='config.yml' will be at
     * '[path to server folder]/plugins/Elementalists/config.yml'. If the file doesn't
     * exist already, then it will be created.
     *
     * @param path  relative location from the data folder to the file
     */
    public ConfigFile(String path) {
        file = new File(Elementalists.pl.getDataFolder().getAbsolutePath() + path);
        if (!file.exists()) {
            try {
                if (!file.createNewFile())
                    Elementalists.log.logError("Failed to create file, " + path + ", for unknown reason!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        yaml = YamlConfiguration.loadConfiguration(file);
    }
    
    /**
     * Loads all data from the config file into relevant objects and data structures
     */
    public abstract void load();
    
    /**
     * Saves data from corresponding objects and data structures to the config file
     */
    public void save() {
        try {
            yaml.save(file);
        } catch (IOException e) {
            Elementalists.log.logError("Failed to save config for " + file.getName() + "!");
        }
    }
    
    /**
     * Get the YamlConfiguration for this particular config.
     *
     * @return  the YamlConfiguration object for the config file
     */
    public YamlConfiguration getConfig() {
        return yaml;
    }
    
    /**
     * Get the raw config file.
     *
     * @return  the raw config file
     */
    public File getFile() {
        return file;
    }
}
