package com.hextaine.elementalists.util.config;
/*
 * Created by Hextaine on 2/2/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.game.Game;

/**
 * The implementation of the {@link ConfigFile} that extends to save Games.
 */
public class GameConfig extends ConfigFile {
    public GameConfig(String path) {
        super(path);
    }

    /**
     * Loads all saved games from the config into the static Games
     * ArrayList.
     */
    @Override
    public void load() {
        for (String game : getConfig().getStringList("games")) {
            Game.games.add(Game.deserialize(game));
        }
    }

    /**
     * Saves all games in the static Games ArrayList (after serializing
     * them) to the config.
     */
    @Override
    public void save() {
        String[] serializedGames = new String[Game.games.size()];
        for (int i = 0; i < serializedGames.length; i++) {
            serializedGames[i] = Game.games.get(i).toString();
        }

        getConfig().set("games", serializedGames);

        super.save();
    }
}
