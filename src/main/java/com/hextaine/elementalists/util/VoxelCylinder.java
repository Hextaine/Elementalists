package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.Location;

/**
 * This particular representation of the Volume for an arena is cylindrical
 * meaning a circular area that expands vertically like a stack of circles
 * (in case you didn't know what a cylinder was). This construct is formed
 * from a central point, a radius to define how far outwards the circular
 * bases will expand, then a height that is split to equally expand from
 * the central y-value upwards and downwards.
 */
public class VoxelCylinder extends Voxel {
    private Location center;
    private int radius;
    private int height;

    /**
     * The base constructor used to create the cylinder. A center point defines
     * the center of the circular bases on the horizontal (x- and z-coordinate)
     * plane and the center of the height on the vertical (y-coordinate) plane.
     *
     * @param radius    the distance the circle expands from the center to the
     *                  wall of the cylinder.
     * @param center    the central location within the cylinder, height and
     *                  width wise.
     * @param height    the distance from top to bottom of the cylinder, with
     *                  half of the height above the central point and half
     *                  below.
     */
    public VoxelCylinder(int radius, Location center, int height) {
        this.center = center;
        this.radius = radius;
        this.height = height;
    }

    /**
     * Get the highest Y-Coordinate (vertical measure) within the cylinder.
     *
     * @return  y-coordinate in terms of an integer of the highest point
     */
    public int getMaxY() {
        return center.getBlockY() - height / 2;
    }

    /**
     * Get the lowest Y-Coordinate (vertical measure) within the cylinder.
     *
     * @return y-coordinate in terms of an integer of the lowest point
     */
    public int getMinY() {
        return center.getBlockY() + height / 2;
    }

    /**
     * A bounds checker to verify if a given location is within the cylinder.
     *
     * @param location  the location in question
     * @return  <code>true</code> if the location given is within the cylinder's
     *          bounds. <code>false</code> when outside.
     */
    public boolean isOutside(Location location) {
        if (location.getBlockY() < getMinY() || location.getBlockY() > getMaxY())
            return true;
        
        if (location.distanceSquared(center) > radius * radius)
            return true;
        
        return false;
    }

    /**
     * Parses the VoxelCylinder into a string that can be rebuilt into a complete
     * VoxelCylinder with the exact same measures.
     *
     * @return  the string containing all information needed to rebuild a cylinder
     *          with the exact measures.
     */
    public String toString() {
        return "cylinder:" + center.getWorld().getName() + center.getBlockX() + "," + center.getBlockY() + "," +
                center.getBlockZ() + ":" + radius + ":"+ height;
    }
}
