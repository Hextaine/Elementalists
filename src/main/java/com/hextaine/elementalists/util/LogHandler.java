package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 6/20/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.plugin.Plugin;

import java.util.logging.Logger;

public class LogHandler {
    private Plugin plugin;
    private Logger log;
    
    public LogHandler(Plugin plugin) {
        this.plugin = plugin;
        this.log = plugin.getLogger();
    }
    
    /**
     * Statically access the logger to make an informative / debugging log message with the
     * appropriate prefixes.
     *
     * @param message   the message that will be logged
     */
    public void logMessage(String message) {
        log.info(message);
    }
    
    /**
     * Statically access the logger to make a sever log message with the appropriate prefixes.
     *
     * @param error     the message that will be logged
     */
    public void logError(String error) {
        log.severe(error);
    }
    
}
