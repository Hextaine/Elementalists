package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/29/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.entity.EntityStats;

public class KnockbackInteractor {

    /**
     * Knocks back any entity specified after resolving any open effects the entity has.
     * This is needed for any effect where knockback is shared, diverted, or canceled.
     *
     * @param entity
     *            the entity that was the target of a knockback action
     * @param direction
     *            the direction of the knockback
     * @param power
     *            the strength of the knockback
     */
    public static void knockBackEntity(Entity entity, Vector direction, double power, EntityStats sourceEntity) {
        EntityStats stats = EntityStats.getStats(entity);

        // Scale the vector down so it's a magnitude of 1, then we scale up to our liking
        double magnitudeSq = direction.getX() * direction.getX() + direction.getY() * direction.getY() + direction.getZ() * direction.getZ();
        direction = direction.multiply(1F / Math.sqrt(magnitudeSq));

        EntityStats target;
        if (stats == null) {
            target = stats;
        } else {
            target = stats.resolveTarget();
        }

        if (target == null) {
            return;
        }

        target.setVelocity(direction.multiply(power), sourceEntity);
    }
}
