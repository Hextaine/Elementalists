package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/25/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

import javax.annotation.Nonnull;


public class InventoryGui implements Listener {
    public static HashMap<String, InventoryGui> activeInventories = null;
    private Inventory inv;
    private Player isBeingEditedBy;
    private boolean isDispenser;
    private boolean clickOnCooldown;

    private final static long CLICKCOOLDOWN = 10;
    
    /**
     * Constructs an empty inventory with the provided size called the provided title.
     * *Note: this inventory will have a <code>null</code> owner.
     *
     * @param size  the number of slots in the inventory
     * @param title what the inventory will be called (title appears at the top of the
     *              inventory gui)
     */
    public InventoryGui(int size, String title) {
        inv = Bukkit.createInventory(null, size, title);
        isBeingEditedBy = null;
        isDispenser = false;
        
        Bukkit.getServer().getPluginManager().registerEvents(this, Elementalists.pl);
        
        if (activeInventories == null)
            activeInventories = new HashMap<>();
        
        activeInventories.put(title, this);
    }
    
    /**
     * Creates an InventoryGUI based on a player's inventory, excluding their hot bar.
     * *Note: The inventory's owner will be reset to <code>null</code>.
     *
     * @param owner     The player who's inventory is being copied.
     * @return          The InventoryGUI copied from the player.
     */
    @Nonnull
    public static InventoryGui copyFromPlayerInventory(Player owner, String title) {
        if (owner == null)
            return new InventoryGui(9, "BAD INVENTORY COPY -- " + title);
        
        if (owner.getInventory() == null)
            return new InventoryGui(9, "BAD INVENTORY COPY -- " + title);
        
        InventoryGui inventoryCopy = new InventoryGui(9  * 3, title);
        
        int invGuiSpace = 0;
        for (int playerInvSpace = 9; playerInvSpace < 36; playerInvSpace++)
            inventoryCopy.set(invGuiSpace++, owner.getInventory().getItem(playerInvSpace));
    
        return inventoryCopy;
    }
    
    /**
     * Handles the deletion of an InventoryGUI.
     *
     * @param title The name of the InventoryGUI that will be deleted.
     */
    public static void deleteInventory(String title) {
        if (title == null)
            return;
        
        if (!activeInventories.containsKey(title))
            return;
        
        activeInventories.remove(title);
    }
    
    /**
     * Sets the specified space in the inventory to the item provided.
     *
     * @param inventorySpace    the place in the inventory you are reseting to the new item
     * @param item              the item to place in the space
     */
    public void set(int inventorySpace, ItemStack item) {
        inv.setItem(inventorySpace, item);
    }
    
    /**
     * Sets an item at a space given by the row and space number that internally resolves to
     * a single number which then acts similarly to the method this overloads: set the space
     * at 9*row + space equal to the item provided.
     * Note: An inventory will have as many rows as the overall number of spaces / 9.
     *
     * @param row           the row (0 for first row)
     * @param spacesAcross  the number of spaces across (0 - 9) to set the item at
     * @param item          the item that will be set into the inventory
     */
    public void set(int row, int spacesAcross, ItemStack item) {
        inv.setItem(row * 9 + spacesAcross, item);
    }
    
    /**
     * Remove all items from all inventory spaces.
     */
    public void clear() {
        inv.clear();
    }
    
    /**
     * Remove any item for a provided space.
     *
     * @param space the space at which will be cleared
     */
    public void clearSpace(int space) {
        inv.clear(space);
    }
    
    /**
     * Remove any items located at a space specified by a row and column.
     *
     * @param row           the row the space that will be cleared is located
     * @param spacesAcross  the column the space that will be cleared is located
     */
    public void clearSpace(int row, int spacesAcross) {
        inv.clear(row * 9 + spacesAcross);
    }
    
    /**
     * Get the Bukkit inventory for the GUI.
     *
     * @return  bukkit inventory used for the GUI
     */
    public Inventory getInventory() {
        return inv;
    }
    
    /**
     * Display the GUI for a player.
     *
     * @param player    the player who will see the GUI
     */
    public void show(Player player) {
        player.openInventory(inv);
    }
    
    /**
     * Displays the GUI for a player, but allows them to change the contents of that
     * inventory. Once the player closes that inventory, the inventory is then saved
     * as the new GUI.
     *
     * @param player    The player the inventory will be open to and allowed to be
     *                  edited by.
     * @return  Whether or not the player was successfully allowed to edit the
     *          inventory.
     */
    public boolean openEditGui(Player player) {
        // Make sure the GUI isn't already being edited
        if (isBeingEditedBy != null)
            return false;
        
        // Check against all nulls
        if (player == null || inv == null)
            return false;
        
        // Open the inventory
        show(player);
        
        // Set the current editing player
        isBeingEditedBy = player;
        return true;
    }
    
    /**
     * Changes the inventory to be either a dispensor inventory or not. A dispensor
     * inventory allows a player to click on an item and copy that item to his/her
     * cursor without actually removing the item from the inventory.
     *
     * @param doesDispense  Whether or not the player is allowed to grab copies of
     *                      the inventory's contents into his/her own inventory.
     */
    public void setDispensor(boolean doesDispense) {
        this.isDispenser = doesDispense;
    }
    
    /**
     * Controls all inventory interactions with this inventory so that you can
     * interact with the items, but you cannot displace, take, or add any items
     * into the inventory unless you are currently editing an inventory.
     * <p>
     * A special note about this method is that it allows a player to use their
     * regular inventory excluding double-click summoning all items and shift
     * clicking items.
     *
     * @param event the InventoryInteractionEvent provided by the EventManager
     */
    @EventHandler (priority = EventPriority.LOW)
    public void cancelGuiInventoryClicking(InventoryClickEvent event) {
        // Ensure the inventory being interacted with is the GUI
        if (!event.getInventory().equals(inv))
            return;
        
        // If the player trying to interact with the inventory is the player who is editing,
        // let them do their thing.
        if (isBeingEditedBy != null && event.getWhoClicked().getUniqueId().equals(isBeingEditedBy.getUniqueId()))
            return;
        
        // Make sure the player cannot click-summon, that the player isn't shift clicking,
        // and that the player is clicking within his/her own inventory.
        if (!clickOnCooldown
                && !event.getClick().isShiftClick()
                && event.getRawSlot() >= inv.getSize()) {
            
            // Allow the event to continue from this listener
            event.setCancelled(false);
            
            // Stop the player from using the double-click item summon by placing a cooldown
            // on how quickly the player can click within their inventory.
            clickOnCooldown = true;
            Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
                public void run() {
                    clickOnCooldown = false;
                }
            }, CLICKCOOLDOWN);
            
            return;
        }
        
        // If none of the previous conditions, cancel the inventory click.
        event.setCancelled(true);
    }
    
    /**
     * Handle when the inventory is set to dispense mode. This means that when a player
     * clicks on an item in the inventory, they should pick up that item into their cursor,
     * but the original item should reappear. The player will not be able to click on the
     * items if he/she has an item in his/her cursor or using any other method that left
     * or right clicking.
     *
     * @param event The specific click event where the player is interacting with an
     *              inventory.
     */
    @EventHandler (priority = EventPriority.NORMAL)
    public void dispenseItems(final InventoryClickEvent event) {
        // Make sure we are dealing with the right inventory
        if (!event.getInventory().equals(inv))
            return;
    
        // Check to see if the inventory can dipense
        if (!isDispenser)
            return;
    
        // See if the person handling the inventory is editing
        if (event.getWhoClicked().equals(isBeingEditedBy))
            return;
    
        // Check to see if the player is adjusting their own inventory or clicking on the GUI
        if (event.getRawSlot() >= inv.getSize())
            return;
        
        // Make sure the player doesn't currently have anything in his/her cursor and isn't
        // shift clicking.
        if ((event.getCursor() != null && !event.getCursor().getType().equals(Material.AIR))
                || event.isShiftClick()) {
            event.setCancelled(true);
            return;
        }
        
        // If none of the above are true, don't cancel the event and let the player take the item
        event.setCancelled(false);
        
        // Setup the item to be replaced after a short delay
        final ItemStack itemToDispense = event.getCurrentItem();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
            public void run() {
                if (event.getRawSlot() < 0)
                    return;
                
                if (event.getRawSlot() < inv.getSize()) {
                    inv.setItem(event.getSlot(), itemToDispense);
                }
            }
        }, 1);

    }
    
    /**
     * Saves an inventory configuration if a player was editing an InventoryGui.
     *
     * @param event The Inventory that was being closed by the editing player.
     */
    @EventHandler
    public void saveInventoryEdits(InventoryCloseEvent event) {
        // Makes sure the player who is closing the inventory is the same as the one
        // currently editing the inventory.
        if (event.getPlayer().equals(isBeingEditedBy)) {
            inv = event.getInventory();
            
            // Reset the player editing the inventory to nobody
            isBeingEditedBy = null;
        }
    }
}
