package com.hextaine.elementalists.util;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Particle.DustOptions;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.spells.Spell;
import com.hextaine.elementalists.spells.SpellType;

/**
 * Created by Hextaine on 12/27/2017.
 * Code is not for redistribution or any other use other than personal.
 * All questions can be directed to hextaine@zoho.com
 */

public class Projectile {
    private String name;
    private double range;
    private double damage;
    private Entity caster;
    private SpellType type;
    private Color color;
    private PotionEffect effect;
    private boolean multipleTargets;
    private int taskNum;
    private Location currentLocation;
    private double blocksTraveled;

    public Projectile() {
        this("NULL", 100, 0, null, SpellType.NONE, Color.WHITE, null, false);
    }

    public Projectile(String name, double range, double damage, Entity caster, SpellType type, Color color,
            PotionEffect effect, boolean piercing) {
        this.name = name;
        this.range = range;
        this.damage = damage;
        this.caster = caster;
        this.type = type;
        this.color = color;
        this.effect = effect;
        this.taskNum = -1;
        multipleTargets = piercing;

        currentLocation = null;
    }

    public void spawnParticleProjectile(final Location l, double blocksPerSecond, final Particle particle) {
        this.spawnParticleProjectile(l, blocksPerSecond, particle, null);
    }

    public void spawnParticleProjectile(final Location l, double blocksPerSecond, final Particle particle, final DustOptions particleData) {
        currentLocation = l.clone();

        // divide by 4 so we can show 4 particles per second
        final double dist = blocksPerSecond / 4;
        final Vector direction = l.getDirection().multiply(dist);
        final boolean infinite = (range == -1);

        this.taskNum = Bukkit.getScheduler().scheduleSyncRepeatingTask(Elementalists.pl, new Runnable() {
            public void run() {
                if (!infinite && range <= 0)
                    Bukkit.getScheduler().cancelTask(taskNum);

                if (l.getBlock().getType().isSolid())
                    Bukkit.getScheduler().cancelTask(taskNum);

                l.add(direction);

                l.getWorld().spawnParticle(particle, l, 1, particleData);

                range -= dist;

                for (Entity e : l.getWorld().getNearbyEntities(l, Spell.PROJECTILETOLERANCE,
                        Spell.PROJECTILETOLERANCE,
                        Spell.PROJECTILETOLERANCE)) {
                    if (!(e instanceof LivingEntity))
                        continue;

                    if (blocksTraveled < 1 && e.equals(caster))
                        continue;

                    if (damage > 0.0)
                        ((LivingEntity) e).damage(damage);

                    if (e instanceof Player) {
                        Elementalists.playerStats.get(e).addEffect(effect);
                    } else {
                        ((LivingEntity) e).addPotionEffect(effect);
                    }

                    if (!multipleTargets) {
                        Bukkit.getScheduler().cancelTask(taskNum);
                        return;
                    }
                }

                blocksTraveled += dist;
            }
        }, 0, 5L);
    }

    public void spawnProjectile(Location l, Material m) {
        // TODO
    }
}
