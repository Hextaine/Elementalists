package com.hextaine.elementalists.util;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.Location;

/**
 * A construct of a {@link Voxel} that encompasses a rectangular prism used
 * to keep track of an arena's volume.
 */
public class VoxelBox extends Voxel {
    private Location minCorner;
    private Location maxCorner;

    /**
     * Constructs a rectangular prism (or at least everything needed to visualize
     * one) from two locations that are assumed to be opposing corners. The two
     * locations given will be reorganized into being a minimum and maximum corner
     * so the provided order doesn't matter.
     *
     * @param loc1  one opposing corner to the rectangular prism
     * @param loc2  another opposing corner for a rectangular prism
     */
    public VoxelBox(Location loc1, Location loc2) {
        double minX, maxX, minY, maxY, minZ, maxZ;
        
        if (loc1.getX() < loc2.getX()) {
            minX = loc1.getX();
            maxX = loc2.getX();
        }
        else {
            minX = loc2.getX();
            maxX = loc1.getX();
        }
        
        if (loc1.getY() < loc2.getY()) {
            minY = loc1.getY();
            maxY = loc2.getY();
        }
        else {
            minY = loc2.getY();
            maxY = loc1.getY();
        }
        
        if (loc1.getZ() < loc2.getZ()) {
            minZ = loc1.getZ();
            maxZ = loc2.getZ();
        }
        else {
            minZ = loc2.getZ();
            maxZ = loc1.getZ();
        }
        
        this.minCorner = new Location(loc1.getWorld(), minX, minY, minZ);
        this.maxCorner = new Location(loc1.getWorld(), maxX, maxY, maxZ);
    }

    /**
     * Gets the maximum vertical value (y-coordinate) possible in integer form to
     * still be within the rectangular prism.
     *
     * @return  the maximum y-coordinate
     */
    public int getMinY() {
        return minCorner.getBlockY();
    }

    /**
     * Gets the minimum vertical value (y-coordinate) possible in integer form to
     * still be within the rectangular prism.
     *
     * @return  the minimum y-coordinate
     */
    public int getMaxY() {
        return maxCorner.getBlockY();
    }

    /**
     * Tests whether a provided location is within the rectangular construct or
     * not.
     *
     * @param location  the provided location in question
     * @return  <code>true</code> if the provided location is within the rectangular
     *          prism. Otherwise return <code>false</code>
     */
    public boolean isOutside(Location location) {
        if (location.getBlockY() < getMinY() || location.getBlockY() > getMaxY())
            return true;
        
        if (location.getBlockX() < minCorner.getBlockX() || location.getBlockX() > maxCorner.getBlockX())
            return true;
        
        if (location.getBlockZ() < minCorner.getBlockZ() || location.getBlockZ() > maxCorner.getBlockZ())
            return true;
        
        return false;
    }

    /**
     * Parses the VoxelBox into a string that contains all necessary information
     * to rebuild the Voxel in order to contain the same volume at the same
     * location.
     *
     * @return  a string containing all necessary information to rebuild the Voxel
     */
    public String toString() {
        return "box:" + minCorner.getWorld().getName()  + "," + minCorner.getBlockX() + "," + minCorner.getBlockY() +
                "," + minCorner.getBlockZ() + ":" + maxCorner.getBlockX() + "," + maxCorner.getBlockY() +
                "," + maxCorner.getBlockZ();
    }
}
