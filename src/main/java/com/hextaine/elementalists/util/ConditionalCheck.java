package com.hextaine.elementalists.util;

public interface ConditionalCheck {
    boolean meetsSelfCancelConditions(Object o);
}
