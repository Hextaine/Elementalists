package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.util.Voxel;
import com.hextaine.elementalists.util.VoxelBox;
import com.hextaine.elementalists.util.VoxelCylinder;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * The construct that encompasses all structural needs for an area: the
 * area the arena encompasses ({@link Voxel}), the spawn locations of the
 * arena, and the identifier.
 */
public class Arena implements Serializable {
    public static ArrayList<Arena> arenas = new ArrayList<Arena>();
    private String id;
    private Voxel voxel;
    private ArrayList<Location> spawns;

    private transient static boolean killOnExit = false;

    /**
     * Constructs a rectangular arena using the two locations given as the
     * bounds for the arena. No order is needed when providing the two
     * corner bounds for the rectangular prism: the will be sorted as
     * needed by the VoxelBox creator.
     *
     * @param id
     *            the (hopefully) unique identifier for this arena
     * @param loc1
     *            one corner for a rectangular arena
     * @param loc2
     *            the opposing corner for the rectangular arena
     * @see VoxelBox
     */
    public Arena(String id, Location loc1, Location loc2) {
        voxel = new VoxelBox(loc1, loc2);
        this.id = id;

        arenas.add(this);
    }

    /**
     * Constructs a cylindrical arena given a unique identifier, the center
     * of the cylinder, the radius, and the height.
     *
     * @param id
     *            the (hopefully) unique identifier
     * @param loc1
     *            the center of the cylinder (height and radius)
     * @param radius
     *            the distance from the center the width of the
     *            cylinder will encompass
     * @param totalHeight
     *            the total height the cylinder will be. -1 will
     *            automatically span bedrock to sky
     */
    public Arena(String id, Location loc1, int radius, int totalHeight) {
        voxel = new VoxelCylinder(radius, loc1, totalHeight);
        this.id = id;

        arenas.add(this);
    }

    public Arena(String id, Voxel voxel) {
        this.voxel = voxel;
        this.id = id;
    }

    public Location[] getSpawns() {
        return spawns.toArray(new Location[arenas.size()]);
    }

    public void addSpawn(Location l) {
        spawns.add(l);
    }

    public void removeSpawn(Location l) {
        spawns.remove(l);
    }

    public String getId() {
        return id;
    }

    public String toString() {
        return id + ";" + voxel.toString();
    }

    public static Arena deserialize(String rawArena) {
        String[] attributes = rawArena.split(";");
        return new Arena(/* id */ attributes[0], Voxel.fromString(attributes[1]));
    }
}
