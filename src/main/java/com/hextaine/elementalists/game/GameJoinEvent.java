package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameJoinEvent extends Event implements Cancellable {
    public static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Game game;
    private PlayerStats player;
    
    /**
     * Constructs the event to pass along the game that the player will join
     * as well as the player that is joining the game.
     *
     * @param game      the game the player will join
     * @param player    the player that is joining a game
     */
    public GameJoinEvent(Game game, PlayerStats player) {
        this.game = game;
        this.player = player;
    }
    
    /**
     * Getter to fetch the game that is being joined.
     *
     * @return  the game being joined
     */
    public Game getGame() {
        return game;
    }
    
    /**
     * Getter to fetch the player that is joining a game.
     *
     * @return the player joining a game
     */
    public PlayerStats getPlayer() {
        return player;
    }
    
    /**
     * Check if the event has been canceled so far.
     *
     * @return  <code>true</code> if the event has been canceled or
     *          <code>false</code> otherwise
     */
    @Override
    public boolean isCancelled() {
        return cancelled;
    }
    
    /**
     * Cancels the event in the most simplistic way: simply holds the value if it was
     * canceled somewhere along the stack of calls. Access this value using isCancelled()
     * if you want to check if it was cancelled somewhere along the stack of calls.
     *
     * @param cancelled the value to set the canceled boolean to
     */
    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
        
        if (cancelled)
            game.removePlayer(player);
        if (!cancelled)
            game.addPlayer(player);
    }
    
    /**
     * Gets the handlers for this event.
     *
     * @return  handlers
     */
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    /**
     * Gets the handlers for this event.
     *
     * @return  handlers
     */
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
