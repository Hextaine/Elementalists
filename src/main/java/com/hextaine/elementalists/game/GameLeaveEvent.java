package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.entity.PlayerStats;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameLeaveEvent extends Event {
    public static final HandlerList handlers = new HandlerList();
    private Game game;
    private PlayerStats player;
    
    public GameLeaveEvent(Game game, PlayerStats player) {
        this.game = game;
        this.player = player;
    }
    
    public Game getGame() {
        return game;
    }
    
    public PlayerStats getPlayer() {
        return player;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
