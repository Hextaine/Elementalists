package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.entity.PlayerStats;
import com.hextaine.elementalists.util.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * Game is the base object to keep track of all {@link PlayerStats} (aka players),
 * the {@link Arena} for each game, the location of the lobby, {@link GameState},
 * and the {@link Messenger} any and all players within each game. Each Game only
 * keeps track of the structure of the Game, not any of the actions, activities,
 * or types of game play happening within.
 * <p>
 * There can be multiple arenas per game and each game is not restricted as to the
 * number of arenas or players allowed.
 */
public class Game implements Serializable {
    public transient static ArrayList<Game> games = new ArrayList<Game>();

    public transient GameState gameState;

    private String id;
    private ArrayList<Arena> arenas;
    private Location lobby;

    private transient ArrayList<PlayerStats> players;
    private transient Arena nextArena;
    private transient Messenger messenger;

    private transient int minNumPlayersToStart = 10; // TODO config setting
    private transient int timeBeforeStart = 10;
    private transient int startingTaskIds = -1;

    /**
     * Simplistic constructor for when a Lobby is of little importance
     * (will need to be added later on).
     *
     * @param id    the (hopefully) unique identifier
     */
    public Game(String id) {
        this(id, null);
    }

    /**
     * This is the base constructor used to create Games.
     *
     * @param id        the (hopefully) unique identifier
     * @param lobby     the location for where players are teleported during a
     *                  'Lobby Stage'
     */
    public Game(String id, Location lobby) {
        this.id = id;
        minNumPlayersToStart = Elementalists.pl.getConfig().getInt("min-players-needed-for-game");

        players = new ArrayList<>();
        arenas = new ArrayList<>();
        messenger = new Messenger();

        this.lobby = lobby;
        nextArena = null;

        gameState = GameState.OFF;

        games.add(this);
    }
    
    /**
     * Gets the identifier of the current game.
     *
     * @return  the game's id
     */
    public String getId() {
        return id;
    }
    
    /**
     * Get the location players are teleported to when in the 'Lobby Stage'.
     * The 'Lobby Stage' is any time players who have joined a game are not currently
     * playing a game: waiting for more players to join before a game can begin, waiting
     * for the game to start, or right after a game has finished.
     *
     * @return  the location of where players should be teleported to for the 'Lobby Stage'
     * @see     Location
     */
    public Location getLobby() {
        return lobby;
    }

    /**
     * Change the location of where players should be taken to while waiting for players to
     * join a game, waiting for the game to start, or right after a game has finished.
     *
     * @param lobby     location players should be teleported to upon reaching a 'Lobby Stage'
     */
    public void setLobby(Location lobby) {
        this.lobby = lobby;
    }

    /**
     * Returns an array of Players wrapped in PlayerStats so in-game operations can be
     * performed with more ease. The method transforms a PlayerStats ArrayList into
     * a PlayerStats[] so order is never guaranteed nor should it be relied on, only the
     * fact that all players in the game are present should be relied on.
     *
     * @return  all players considered active in the game
     * @see PlayerStats
     */
    public PlayerStats[] getPlayers() {
        PlayerStats[] participants = new PlayerStats[players.size()];
        return players.toArray(participants);
    }

    /**
     * Registers a player to the game's PlayerStats ArrayList after making sure the entry
     * is unique. When the provided player is seen as a unique player (not already existing
     * within the ArrayList), a GameJoinEvent is called. The PlayerStats is also changed
     * to reflect that he/she is currently in this game and is added to the Messenger for
     * game-wide messages.
     *
     * @param player    the PlayerStats representation of a player, attempting to join
     * @see     GameJoinEvent
     * @see     Messenger
     */
    public void addPlayer(PlayerStats player) {
        if (players.contains(player))
            return;

        Elementalists.pl.getServer().getPluginManager().callEvent(new GameJoinEvent(this, player));

        players.add(player);
        player.setCurrentGame(this);
        messenger.add((Player)player.getEntity());
    }

    /**
     * After checking to make sure the player is listed as a player in this game,
     * a GameLeaveEvent is called and the player is removed from the PlayerStats
     * ArrayList as well as the game's Messenger.
     *
     * @param player    the PlayerStats representation of the player attempting to leave
     * @see     GameLeaveEvent
     * @see     Messenger
     */
    public void removePlayer(PlayerStats player) {
        if (!players.contains(player))
            return;

        Elementalists.pl.getServer().getPluginManager().callEvent(new GameLeaveEvent(this, player));
        
        players.remove(player);
        if (player.getCurrentGame() != null) {
            player.setCurrentGame(null);
        }

        messenger.remove((Player) player.getEntity());
    }

    /**
     * Adds an arena to the Game's Arena rotation. A Game can in fact have an unlimited
     * number of arenas.
     *
     * @param arena     the arena to add to the rotation
     */
    public void addArena(Arena arena) {
        arenas.add(arena);
    }

    /**
     * Removes an arena from the Game's Arena rotation. If the arena isn't contained within
     * the rotation, nothing will be changed.
     *
     * @param arena     the arena to be removed from the rotation.
     */
    public void removeArena(Arena arena) {
        arenas.remove(arena);
    }

    /**
     * Moves the game into a Waiting state that waits for enough players to join the
     * game. It is assumed the game will go into its Active state once enough players
     * have joined the game.
     *
     * @return  <code>false</code> if there are no arenas, otherwise it returns
     *          <code>true</code>
     * @see GameState
     * @see GameJoinEvent
     */
    public boolean activate() {
        if (arenas.size() <= 0) return false;
        
        Random r = new Random();
        nextArena = arenas.get(r.nextInt(arenas.size()));
        
        gameState = GameState.WAITING;
        return true;
    }

    /**
     * Changes the game's state to be Active, resets all players' stats, then teleports
     * all players to spawn points throughout the arena, overlapping on spawn locations
     * when there are more players than spawn points.
     * <p>
     * This will also call a GameStartEvent.
     *
     * @see GameState
     * @see GameStartEvent
     */
    public void start() {
        if (nextArena == null)
            return;

        gameState = GameState.ACTIVE;
        Bukkit.getPluginManager().callEvent(new GameStartEvent(this));

        Location[] spawns = nextArena.getSpawns();

        int spawnNum = 0;
        for (PlayerStats player : players) {
            player.resetGameStats();
            player.getEntity().teleport(spawns[spawnNum++ % spawns.length]);
        }
    }

    /**
     * Officially ends the game if it isn't already over, calling a GameEndEvent
     * and changing the game's state to Waiting. All players in game will have
     * their 'stats' reset and will be teleported to the lobby location. The
     * next arena will be selected and the game will check if it can start again.
     * If it cannot start due to insufficient number of players, the
     * GameJoinEvent will kick off starting this game.
     *
     * @see GameState
     * @see GameEndEvent
     */
    public void stop() {
        if (!gameState.equals(GameState.ACTIVE))
            return;

        gameState = GameState.WAITING;
        Bukkit.getPluginManager().callEvent(new GameEndEvent(this));

        for (PlayerStats player : players) {
            player.resetGameStats();
            player.getEntity().teleport(getLobby());
        }

        Random r = new Random();
        nextArena = arenas.get(r.nextInt(arenas.size()));

        if (players.size() >= minNumPlayersToStart)
            beginStartSequence();
    }

    /**
     * Displays a countdown to players within the game so that once the game
     * has fulfilled all starting requirements, the players can have some time
     * to mentally prepare for the game to begin. Once the countdown finishes,
     * the Game will start.
     */
    public void beginStartSequence() {
        startingTaskIds = Bukkit.getScheduler().scheduleSyncRepeatingTask(Elementalists.pl, new Runnable() {
                    public void run() {
                        messenger.sendPluginMessage("Game will begin in " + timeBeforeStart);
                        timeBeforeStart--;

                        if (timeBeforeStart <= 1){
                            start();
                            cancelStartSequence();
                        }
                    }
                }, 0L, 20L);
    }

    /**
     * Stops all tasks
     */
    public void cancelStartSequence() {
        if (startingTaskIds <= 0)
            return;

        Bukkit.getScheduler().cancelTask(startingTaskIds);

        timeBeforeStart = 10;

        startingTaskIds = -1;
    }

    /**
     * Get the Messenger all players in the game are connected to. The Messenger is
     * most often used to broadcast kill messages, time limits, Game updates, or
     * similar messages all players within a game need to hear.
     *
     * @return  the Messenger used to broadcast messages to players in the Game
     */
    public Messenger getMessenger() {
        return messenger;
    }

    /**
     * Serializes the Game's id (already a string) and lobby location to an easily parsable
     * format. This does not include the Arena rotation the Game has. This is serialized
     * separately.
     * <p>
     * String format: [id];[world name],[x],[y],[z]
     *
     * @return  the string form of the Id and location of the lobby
     */
    public String toString() {
        return id + ";" + lobby.getWorld().getName() + "," + lobby.getX() + "," + lobby.getY() + "," + lobby.getZ();
    }

    /**
     * Parses a string in the same format laid out in the toString method. When a string
     * is provided not in that format or is invalidly formatted, this method will return
     * <code>null</code>.
     *
     * @param   rawGame   the serialized game string
     * @return  the Game parsed from the string or <code>null</code>
     */
    public static Game deserialize(String rawGame) {
        String[] attributes = rawGame.split(";");

        if (attributes.length < 2)
            return null;

        String[] worldXYZ = attributes[1].split(",");

        Location lobby = null;
        try {
            lobby = new Location(Bukkit.getWorld(worldXYZ[0]), Double.parseDouble(attributes[1]),
                    Double.parseDouble(attributes[2]), Double.parseDouble(attributes[3]));
        } catch (Exception e) {
            Elementalists.log.logError("Failed to parse lobby location for Game, " + attributes[0] + ":" +
                    e.getMessage());
            e.printStackTrace();
        }


        return new Game(attributes[0], lobby);
    }

    /**
     * Returns the number of players currently registered to this game.
     *
     * @return  the number of players in this game
     */
    public int getNumPlayers() {
        return players.size();
    }

    /**
     * Gets the minimum number of players needed before this game is allowed to start.
     *
     * @return  the number of players needed.
     */
    public int getMinNumPlayers() {
        return minNumPlayersToStart;
    }
    
    /**
     * Checks if the provided ID exists as a game already.
     *
     * @param gameId    the id of the game you are looking for
     * @return  <code>true</code> if the game exists or <code>false</code> if it doesn't
     */
    public static boolean gameExists(String gameId) {
        for (Game game : games)
            if (game.getId().equalsIgnoreCase(gameId))
                return true;
        
        return false;
    }
    
    /**
     * Gets the game given the game's identifier. If a game doesn't currently exist with that
     * identifier, <code>null</code> is returned.
     *
     * @param gameId    the id of the game you are looking for
     * @return  the Game object that has the given id or <code>null</code> if no games have that
     *          ID
     */
    public static Game getGame(String gameId) {
        for (Game game : games)
            if (game.getId().equalsIgnoreCase(gameId))
                return game;
        
        return null;
    }
}
