package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/19/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStartEvent extends Event {
    public static final HandlerList handlers = new HandlerList();
    private Game game;
    
    public GameStartEvent(Game g) {
        this.game = g;
    }
    
    public Game getGame() {
        return game;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
