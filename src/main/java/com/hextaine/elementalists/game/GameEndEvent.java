package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/19/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEndEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Game game;
    
    public GameEndEvent(Game game) {
        this.game = game;
    }
    
    public Game getGame() {
        return game;
    }
    
    @Override
    public HandlerList getHandlers() {
        return null;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
