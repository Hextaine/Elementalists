package com.hextaine.elementalists.game;
/*
 * Created by Hextaine on 12/17/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

/**
 * An enumeration over the possible states of the game. Used as
 * constants in order to track the current stage of a game.
 */
public enum GameState {
    ACTIVE, WAITING, ENDING, STARTING, OFF;
    
    GameState() {}
}
