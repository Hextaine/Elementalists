package com.hextaine.elementalists.entity;
/*
 * Created by Hextaine on 1/13/2018.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import com.hextaine.elementalists.Elementalists;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class RecentlyCastHandler {
    private int[] usage;
    private ArrayList<BukkitRunnable> timers;
    
    /**
     * Creates a new RecentlyCast object which tracks the usage of spells across
     * the 9 item slots a player has to cast spells with.
     */
    public RecentlyCastHandler() {
        usage = new int[9];
        timers = new ArrayList<>();
    }
    
    /**
     * Registers a spell was cast and increments the recently cast slot so that
     * the next use of the spell in quick succession correctly implements the
     * exhaustion system.
     *
     * @param slot          the slot of the hotbar the spell was cast from
     * @param exhaustRate   the spell's exhaustion rate that increases the time
     *                      the exhaustion per cast of the spell takes to wear off
     * @see com.hextaine.elementalists.spells.Spells
     */
    public void castedSpell(final int slot, long exhaustRate) {
        usage[slot]++;
    
        BukkitRunnable decrement = new BukkitRunnable() {
            public void run() {
                usage[slot]--;
                timers.remove(this);
            }
        };
    
        timers.add(decrement);
        decrement.runTaskLater(Elementalists.pl, exhaustRate);
    }
    
    /**
     * Resets all timers regarding exhaustion and recently cast spells as if
     * no spells have been cast recently. Usually used when a game ends or
     * if a player finds a way to reset all exhaustion.
     */
    public void reset() {
        for (int i = 0; i < usage.length; i++)
            usage[i] = 0;
            
        for (BukkitRunnable br : timers) {
            br.cancel();
        }
    }
}
