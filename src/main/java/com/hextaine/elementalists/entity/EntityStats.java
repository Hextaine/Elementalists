package com.hextaine.elementalists.entity;
/*
 * Created by Hextaine on 12/20/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.afflictions.Affliction;
import com.hextaine.elementalists.afflictions.AfflictionType;

/**
 * The basic construct for keeping track of all Elementalists statistics and information regarding
 * entities. Any entity that can be harmed, buffed, or affected in any way by this plugin must be
 * registered to an EntityStats, including Players ({@link PlayerStats}).
 */
public class EntityStats {
    private Entity entity;
    private double maxHp;
    private transient double hp;
    private transient List<PotionEffect> activeEffects;
    private EntityStats lastDamagedBy;
    private String lastDamageSource;

    public static final String METALABEL = "stats";

    /**
     * Create new EntityStats for a given Bukkit entity.
     *
     * @param entity
     *            the entity that will be instantiated as an Elementalists' entity
     */
    public EntityStats(Entity entity) {
        this.entity = entity;

        if (entity instanceof LivingEntity) {
            this.maxHp = ((LivingEntity) entity).getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        } else {
            this.maxHp = 20;
        }

        this.hp = maxHp;
        this.activeEffects = Collections.synchronizedList(new ArrayList<PotionEffect>());
    }

    /**
     * Retrieves the EntityStats for any given entity, alive or not.
     *
     * @param entity
     *            the Bukkit entity that the entity stats will be retrieved from
     * @return the EntityStats of the entity provided if it is possible or
     *         <code>null</code> otherwise
     */
    public static EntityStats getStats(Entity entity) {
        EntityStats stats = null;
        if (entity.hasMetadata(EntityStats.METALABEL)) {
            for (MetadataValue meta : entity.getMetadata(EntityStats.METALABEL)) {
                if (meta.value() instanceof EntityStats) {
                    return (EntityStats) meta.value();
                }
            }
        }

        if (entity instanceof Player) {
            stats = new PlayerStats((Player) entity);
        } else {
            stats = new EntityStats(entity);
        }

        entity.setMetadata(METALABEL, new FixedMetadataValue(Elementalists.pl, stats));
        return stats;
    }

    /**
     * Resolves the entity that a targeted spell might change targets to if the current
     * entity has any spell active that allows targeted spells to affect another entity.
     *
     * @return the entity the spell will target
     * @see Affliction
     */
    public EntityStats resolveTarget() {
        // for (PotionEffect effect : activeEffects) {
        // if (effect.getType().getName().equalsIgnoreCase("redirect")) {
        // UUID playerId = UUID.fromString(effect.getType().toString());
        // EntityStats stats = EntityStats.getStats(Bukkit.getEntity(playerId));

        // return stats;
        // }
        // }

        return this;
    }

    /**
     * Getter to retrieve the Bukkit Entity of this wrapper.
     *
     * @return the Bukkit Entity
     */
    public Entity getEntity() {
        return entity;
    }

    public double getHp() {
        return hp;
    }

    /**
     * Increase the health of an entity. Any amount of healing not done through this method
     * may be detrimental to how the game registers the health of an entity.
     *
     * @param health
     *            the amount of health being restored to the entity
     */
    public void heal(double health) {
        EntityStats stats = resolveTarget();
        if (stats == null)
            return;

        stats.hp += health;
        if (stats.hp >= 20)
            stats.hp = 20;
    }

    /**
     * Damages the entity updating health, potion effects, the entity, or any events that
     * need to be updated / registered. This method handles all damage that the game and
     * plugin is allowed to register. Any damage that occurs outside of this may be
     * detrimental to how the game registers the health of any given entity.
     *
     * @param dmg
     *            the amount of damage being delt to this entity
     * @param damager
     *            the entity causing the damage
     * @param damageSource
     *            the source of the damage (spell, fire, melee, etc)
     * @see com.hextaine.elementalists.spells.Spells
     */
    public void damage(double dmg, EntityStats damager, String damageSource) {
        EntityStats stats = resolveTarget();
        if (stats == null)
            return;

        lastDamagedBy = damager;
        this.lastDamageSource = damageSource;

        stats.hp -= dmg;
        if (stats.hp <= 0)
            stats.hp = 0;

        if (stats.getEntity() instanceof LivingEntity) {
            double hpRatio = dmg / maxHp;
            ((LivingEntity) stats.getEntity()).damage(hpRatio * 20, damager.getEntity());
        }
    }

    /**
     * Proxy to set the number of ignition ticks in terms of seconds for the entity. Note, if
     * the entity is already on fire, this will change the length of the fire to be equal to
     * whichever number of fire ticks is greater: the one currently registered to the entity
     * or this one.
     *
     * @param newFireTicks
     *            the number of ticks the entity will be set on fire for
     */
    public void ignite(int newFireTicks) {
        if (entity.getFireTicks() > newFireTicks)
            return;
        entity.setFireTicks(newFireTicks);
    }

    /**
     * Proxy boolean check to make sure the entity we have is on fire or not.
     *
     * @return <code>true</code> if the number of fire ticks for the entity is greater than 0
     *         and <code>false</code> otherwise
     */
    public boolean isOnFire() {
        return (entity.getFireTicks() <= 0);
    }

    /**
     * Getter for all active effects currently applied to the entity.
     *
     * @return list of PotionEffects that are registered to the entity
     * @see Affliction
     */
    public List<PotionEffect> getActiveEffects() {
        return activeEffects;
    }

    public boolean hasActivePotionOfType(PotionEffectType potionEffectType) {
        for (PotionEffect pe : getActiveEffects()) {
            if (pe.getType().getName() == potionEffectType.getName()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Adds an effect to the current list of active effects on the entity.
     *
     * @param effect
     *            PotionEffect that is being added
     * @see Affliction
     */
    public void addEffect(PotionEffect effect) {
        EntityStats target = this.resolveTarget();

        target.removeEffectByType(effect.getType());

        target.activeEffects.add(effect);
        if (!(effect instanceof Affliction)) {
            ((LivingEntity) target.getEntity()).addPotionEffect(effect);

        } else {
            PotionEffectType pe = effect.getType();
            if (pe instanceof AfflictionType) {
                AfflictionType at = (AfflictionType) pe;
                at.applyEffect(target);
            }
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Elementalists.pl, new Runnable() {
            public void run() {
                if (target.getActiveEffects().contains(effect))
                    target.removeEffect(effect);
            }
        }, effect.getDuration());
    }

    /**
     * Removes a specific effect from the entity. If the effect has more than one connection to
     * entities (diffuser of damage), it will undo any links that are made through the effect
     * as well.
     *
     * @param effect
     *            the effect that is being canceled
     * @return <code>true</code> if the effect was successfully removed, <code>false</code>
     *         otherwise.
     */
    public boolean removeEffect(PotionEffect effect) {
        if (effect instanceof Affliction && effect.getType() instanceof AfflictionType) {
            ((AfflictionType) effect.getType()).endEffect();
        } else if (entity instanceof LivingEntity) {
            ((LivingEntity) entity).getActivePotionEffects().removeIf((potionEffect) -> potionEffect.getType().getName() == effect.getType().getName());
        }

        return this.activeEffects.remove(effect);
    }

    public boolean removeEffectByType(PotionEffectType effectType) {
        ArrayList<PotionEffect> effectsToRemove = new ArrayList<>();
        getActiveEffects().forEach((activeEffect) -> {
            if (activeEffect.getType().getName() == effectType.getName()) {
                effectsToRemove.add(activeEffect);
            }
        });

        for (PotionEffect pe : effectsToRemove) {
            removeEffect(pe);
        }

        return effectsToRemove.size() > 0;
    }

    /**
     * Clears all effects from the current entity without any checks. Just a simple clearing.
     */
    public void removeAllEffects() {
        for (PotionEffect e : activeEffects) {
            removeEffect(e);
        }
    }

    /**
     * Getter to retrieve the entity that last inflicted damage to this entity.
     *
     * @return the entity that last caused damage to this entity
     */
    public EntityStats getLastDamager() {
        return lastDamagedBy;
    }

    /**
     * Getter to retrieve the source of the last known damage source
     *
     * @return a string containing the name of spell, attack, or general cause of damage
     */
    public String getLastDamageSource() {
        return lastDamageSource;
    }

    /**
     * A supplemental knockback source so that the effect of being knocked back can be tracked
     * for the reason of someone or something's demise.
     *
     * @param vector
     *            the vector that will push the entity
     * @param entitySource
     *            the entity that caused this knockback
     * @see com.hextaine.elementalists.util.KnockbackInteractor
     */
    public void setVelocity(Vector vector, EntityStats entitySource) {
        // TODO spell source to track lastDamageSource?
        entity.setVelocity(vector);
        this.lastDamagedBy = entitySource;
    }
}
