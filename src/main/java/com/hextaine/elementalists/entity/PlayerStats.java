package com.hextaine.elementalists.entity;
/*
 * Created by Hextaine on 12/29/2017.
 * Please give credit where credit is due, when it is due.
 * All questions can be directed to Hextaine@zoho.com
 * Enjoy!
 */

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.PermissionAttachmentInfo;

import com.hextaine.elementalists.Elementalists;
import com.hextaine.elementalists.database.DbPlayerStats;
import com.hextaine.elementalists.game.Game;
import com.hextaine.elementalists.spells.Spells;

public class PlayerStats extends EntityStats {
    private Player player;
    private DbPlayerStats dbPlayerStats;
    private transient double energy;
    private int kills;
    private int deaths;
    private ArrayList<Spells> knownSpells;
    public transient RecentlyCastHandler recentlyCast;
    private Game currentGame;

    public PlayerStats(Player player) {
        super(player);
        this.player = player;
        this.energy = 100;
        this.kills = 0;
        this.deaths = 0;

        knownSpells = new ArrayList<>();
        recentlyCast = new RecentlyCastHandler();
    }

    public static PlayerStats getStats(Player player) {
        if (!player.hasMetadata(EntityStats.METALABEL)) {
            return createStats(player);
        }

        PlayerStats stats = null;

        for (MetadataValue meta : player.getMetadata(EntityStats.METALABEL)) {
            meta.invalidate();
            if (meta.value() instanceof PlayerStats) {
                stats = (PlayerStats) meta.value();
            } else {
                stats = createStats(player);
            }
        }

        return stats;
    }

    public static PlayerStats createStats(Player player) {
        PlayerStats newStatsForPlayer = new PlayerStats(player);

        player.setMetadata(METALABEL,
                new FixedMetadataValue(Elementalists.pl, newStatsForPlayer));

        return newStatsForPlayer;
    }

    @Override
    public Player getEntity() {
        return player;
    }

    public double getEnergy() {
        return energy;
    }

    public void spendEnergy(double energy) {
        this.energy -= energy;
        if (this.energy >= 100)
            this.energy = 100;
    }

    public void regainEnergy(double energy) {
        this.energy -= energy;
        if (this.energy <= 0)
            this.energy = 0;
    }

    public int getKills() {
        return kills;
    }

    public void incrementKills() {
        this.kills += 1;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void incrementDeaths() {
        this.deaths += 1;
    }

    /**
     * Changes the number of deaths the player currently has to a new value.
     *
     * @param deaths
     *            the number of deaths the player will have
     */
    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    /**
     * Get all spells that the player knows how to cast.
     *
     * @return the ArrayList of Spells that the player is permitted to cast
     */
    public ArrayList<Spells> getKnownSpells() {
        return knownSpells;
    }

    /**
     * Gives a player the permission to cast a given spell.
     *
     * @param spell
     *            the spell that the player will 'learn' (get permission to cast)
     */
    public void teachSpell(Spells spell) {
        knownSpells.add(spell);
        player.addAttachment(Elementalists.pl, spell.getSpellHandler().getPermissionString(), true);
        player.recalculatePermissions();
    }

    /**
     * Causes a player to lose the ability to cast a spell. Effectively, the player loses the permission
     * to cast the spell.
     *
     * @param spell
     *            the spell that the player should forget
     * @return <code>true</code> if the player successfully forgot the spell and <code>false</code>
     *         if the player didn't know the spell in the first place
     */
    public boolean forgetSpell(Spells spell) {
        knownSpells.remove(spell);
        for (PermissionAttachmentInfo permInfo : player.getEffectivePermissions()) {
            if (permInfo.getPermission().equals(spell.getSpellHandler().getPermissionString())) {
                player.removeAttachment(permInfo.getAttachment());
                player.recalculatePermissions();
                return true;
            }
        }

        return false;
    }

    /**
     * Transfers the knowledge of a spell from the current player to the provided player. The
     * player transferring the spell will forget the spell while the provided player learns it.
     *
     * @param recipient
     *            the player the spell will be taught to
     * @param spell
     *            the spell that the is being transferred
     */
    public void transferSpell(PlayerStats recipient, Spells spell) {
        if (forgetSpell(spell)) {
            recipient.teachSpell(spell);
        }
    }

    /**
     * Convenience method to check if a player has permission (usually for a spell) but exists due to
     * the future possibility of Elementalists having its own way to track permissions for it own stuff.
     *
     * @param permission
     *            the permission in question of the player having
     * @return <code>true</code> if the player has the permission and <code>false</code> otherwise
     */
    public boolean hasPermission(String permission) {
        if (player.hasPermission(permission))
            return true;
        return false;
    }

    /**
     * Upload any residual stats to the database. The update is additive so it is okay to upload stats
     * as many times as you wish because the relevant stats are reset upon uploading.
     */
    public void uploadGameStats() {
        // TODO save player stats
        // SqlLiteDb.incrementValue(this.player.getUniqueId().toString(), DatabaseVariable.KILLS, kills);
        // SqlLiteDb.incrementValue(this.player.getUniqueId().toString(), DatabaseVariable.DEATHS, deaths);
        resetGameStats();
    }

    /**
     * Checks if the player is currently in a game.
     *
     * @return <code>true</code> if a player is registered to a game or <code>false</code> if the
     *         player doesn't have a game registered to him/her.
     */
    public boolean isInGame() {
        return (currentGame == null);
    }

    /**
     * Gets the current game the player is registered to
     *
     * @return the game object the player is registered to
     */
    public Game getCurrentGame() {
        return currentGame;
    }

    /**
     * Change the current game of the player to be the provided game.
     *
     * @param game
     *            the game the player will be registered to
     */
    public void setCurrentGame(Game game) {
        currentGame = game;
    }

    /**
     * Resets the player's currently registered game.
     */
    public void leaveCurrentGame() {
        currentGame = null;
    }

    /**
     * Reset all game-relevant statistics to their default values and remove any game-effects such
     * as the Recent-Cast buffer that tracks efficiency of a spell as well as the effects on a
     * player from spells cast on him/her.
     */
    public void resetGameStats() {
        kills = 0;
        deaths = 0;
        recentlyCast.reset();
        removeAllEffects();
    }
}
